function checkAnswer(form) {
    deactivateSendButton();
    const url = form.action;
    const data = {
        answer: form.answer.value,
        _token: form._token.value
    };
    fetch(
        url,
        {
            headers: { "Content-Type": "application/json" },
            method: "POST",
            body: JSON.stringify(data)
        }
    )
    .then(response => response.json())
    .then((result) => {
        refreshGame(result);
    })
    .catch((error) => {
        console.error("Error:", error);
        activateSendButton();
    });
    return false;
}


/**
 * Activate the button to send an answer
 */
function activateSendButton() {
    var button = document.getElementById('guess-grid-submit');
    showLoading(button, false);
    button.disabled = false;
    var jersey = document.getElementById('loading-jersey');
    showLoading(jersey, false);
}

/**
 * Deactivate the button to send an answer
 */
function deactivateSendButton() {
    var button = document.getElementById('guess-grid-submit');
    showLoading(button, true);
    button.disabled = true;
    var jersey = document.getElementById('loading-jersey');
    showLoading(jersey, true);
}

/**
 * Show or hide loading animation
 */
function showLoading(item, show) {
    if (show) {
        item.classList.add('loading');
    } else {
        item.classList.remove('loading');
    }
}

/**
 * Update game with result of answer
 */
function refreshGame(status)
{
    if ('over' == status.step) {
        refreshGameOver(status);
        activateSendButton();
    } else {
        refreshGameWithContinue(status);
    }
}

/**
 * Update page with new champion to continue game
 */
function refreshGameWithContinue(status)
{
    // Reset form
    document.getElementById('game-form').reset();

    // Update score
    var scoreEl = document.getElementById('game-score');
    scoreEl.innerHTML = status.score;

    // Write answer
    writeCorrectAnswer(status, '');

    // Wait for images
    var promiseArray = [];
    // Wait for base jersey
    promiseArray.push(new Promise(resolve => {
        const img = new Image();
        img.onload = resolve;
        img.src = status.baseJersey;
    }));
    // Wait for left
    if (status.leftJersey) {
        promiseArray.push(new Promise(resolve => {
            const img = new Image();
            img.onload = resolve;
            img.src = status.leftJersey;
        }));
    }
    // Wait for right
    if (status.rightJersey) {
        promiseArray.push(new Promise(resolve => {
            const img = new Image();
            img.onload = resolve;
            img.src = status.rightJersey;
        }));
    }
    // Wait for collar
    if (status.collarJersey) {
        promiseArray.push(new Promise(resolve => {
            const img = new Image();
            img.onload = resolve;
            img.src = status.collarJersey;
        }));
    }
    Promise.all(promiseArray)
    // Once all images are loaded
    .then(() => {
        console.log("PBO resolu");
        // Update championship
        var championshipCompEl = document.getElementById('championship-id');
        championshipCompEl.innerHTML = status.championship;
        if ('women' == status.championship) {
            var menEl = document.getElementById('championship-men');
            menEl.style.display = 'none';
            var womenEl = document.getElementById('championship-women');
            womenEl.style.display = 'block';
        } else {
            var womenEl = document.getElementById('championship-women');
            womenEl.style.display = 'none';
            var menEl = document.getElementById('championship-men');
            menEl.style.display = 'block';
        }

        // Update jersey
        var jerseyEl = document.getElementById('game-jersey');
        jerseyEl.src = status.baseJersey;
        var leftJerseyEl = document.getElementById('left-jersey');
        if (!status.leftJersey) {
            leftJerseyEl.style.display = 'none';
        } else {
            leftJerseyEl.style.display = 'inline';
            leftJerseyEl.src = status.leftJersey ?? '';
        }
        var rightJerseyEl = document.getElementById('right-jersey');
        if (!status.rightJersey) {
            rightJerseyEl.style.display = 'none';
        } else {
            rightJerseyEl.style.display = 'inline';
            rightJerseyEl.src = status.rightJersey ?? '';
        }
        var collarJerseyEl = document.getElementById('collar-jersey');
        if (!status.collarJersey) {
            collarJerseyEl.style.display = 'none';
        } else {
            collarJerseyEl.style.display = 'inline';
            collarJerseyEl.src = status.collarJersey ?? '';
        }

        // Focus on answer input
        var answerInputEl = document.getElementById('i-answer');
        answerInputEl.focus();

        // Activate Send button
        activateSendButton();
    })
    .catch((error) => {
        console.log(error);
        activateSendButton();
    });
}

/**
 * Update page after right answer and game is not over
 */
function refreshGameOver(status)
{
    switchJerseyGameOver();
    showGameOverZone(status);
    hideGuessZone();
    hideStatusZone();
    hideChampionship();
    showNewGameZone();
}

/**
 * Show the win area
 */
function showGameOverZone(status)
{
    writeCorrectAnswer(status, 'last-');
    writeResultMessage(status);
    writeResultScore(status);
    writeResultStars(status);
    writeResultToCopy(status);
    showResultZone();

}

/**
 * Write correct answer
 */
function writeCorrectAnswer(status, prefix)
{
    var zoneEl = document.getElementById(prefix + 'correction');
    zoneEl.classList.remove('no');
    zoneEl.classList.remove('yes');
    zoneEl.classList.add(status.correct);
    var correctEl = document.getElementById(prefix + 'correct-rider');
    correctEl.innerHTML = status.answer ?? '';
    var iconEl = document.getElementById(prefix + 'correct-icon');
    iconEl.innerHTML = ('yes' == status.correct ? '✔️' : '❌');
}

/**
 * Switch the jersey to game over mode
 */
function switchJerseyGameOver()
{
    var jerseyEl = document.getElementById('jersey-type');
    jerseyEl.classList.remove('in-game');
    jerseyEl.classList.add('after-game');
}

function hideChampionship()
{
    var champEl = document.getElementById('championship');
    champEl.style.display = 'none';
}

/**
 * Hide the usual game area
 */
function hideGuessZone()
{
    var gameEl = document.getElementById('playing-zone');
    gameEl.style.display = 'none';
}

/**
 * Hide the status zone
 */
function hideStatusZone()
{
    var statusEl = document.getElementById('status-grid');
    statusEl.style.display = 'none';
}

/**
 * Show buttons to play a new game
 */
function showNewGameZone()
{
    var newGameEl = document.getElementById('new-game-zone');
    newGameEl.style.display = 'flex';
}

/**
 * Show result zone
 */
function showResultZone()
{
    var resultEl = document.getElementById('result-zone');
    resultEl.style.display = 'grid';
}

/**
 * Write result message
 */
function writeResultMessage(status)
{
    var resultEl = document.getElementById('over-message');
    resultEl.innerHTML = status.message ?? 'Win!';
}

/**
 * Write score in result zone
 */
function writeResultScore(status)
{
    var scoreEl = document.getElementById('result-game-score');
    scoreEl.innerHTML = status.score ?? 0;
}

/**
 * Write stars in result zone
 */
function writeResultStars(status)
{
    const starCount = status.stars ?? 2;
    var stars = '';
    for (let i=0; i<5; ++i) {
        if (i <= starCount) {
            stars += '<span class="on">★</span>';
        } else {
            stars += '<span class="off">★</span>'
        }
    }
    var starsEl = document.getElementById('star-rating');
    starsEl.innerHTML = stars;
}

/**
 * Write the result as it will be copied to clipboard if user wants it
 */
function writeResultToCopy(status)
{
    const safetyMessage = 'I have a score of ' + (status.score ?? 'a lot') + ' on Wholeur at https://wholeur.terrylaire.fr';
    var shareElem = document.getElementById('share-content');
    shareElem.innerHTML = status.resultToCopy ?? safetyMessage; 
}

/**
 * Get current championship ID
 */
function getCurrentChampionship()
{
    var championshipEl = document.getElementById('championship-id');
    return championshipEl.innerHTML ?? 'men';
}

/**
 * Copy result to clipboard
 */
function copyResult(button) {
    const text = document.getElementById('share-content').innerHTML ?? 'I played Wholeur at https://wholeur.terrylaire.fr';
    /** Adapted from https://github.com/sudodoki/copy-to-clipboard */
    var range = document.createRange();
    var selection = document.getSelection();
    var mark = document.createElement("span");
    mark.textContent = text;
    mark.ariaHidden = "true"
    mark.style.all = "unset";
    mark.style.position = "fixed";
    mark.style.top = 0;
    mark.style.clip = "rect(0, 0, 0, 0)";
    mark.style.whiteSpace = "pre";
    mark.style.webkitUserSelect = "text";
    mark.style.MozUserSelect = "text";
    mark.style.msUserSelect = "text";
    mark.style.userSelect = "text";

    document.body.appendChild(mark);

    range.selectNodeContents(mark);
    selection.addRange(range);

    if(document.execCommand("copy")) {
        button.innerHTML = "OK";
    }
}

var accordion = document.getElementsByClassName('faq-question');

for (var i = 0; i < accordion.length; ++i) {
    accordion[i].addEventListener('click', function() {
        this.classList.toggle('active');
        var answer = this.nextElementSibling;
        answer.style.display = 'block' == answer.style.display ? 'none' : 'block';
    });
}

window.copyResult = copyResult;
window.checkAnswer = checkAnswer;
window.getCurrentChampionship = getCurrentChampionship;