<x-layout>
    <x-slot:nav>
        <div id="nav"><a href="{{ route('home') }}"><img src='/img/wholeurheader.png' alt="Wholeur" height="80" width="160"/></a></div>
    </x-slot>

    <div id="all-champions-list">
        <h2>{{ __('tr.all-champions-title')}}</h2>
        <p id="subtitle">{{ __('tr.all-champions-subtitle')}}</p>
        <p id="cta-play"><a href="{{ route('home') }}">{{ __('tr.all-champions-play') }}</a></p>
        <div id="champions">
        @foreach ($champions as $champion)
            @php
                $left = $champion->getLeftJerseyUrl();
                $right = $champion->getRightJerseyUrl();
                $collar = $champion->getCollarJerseyUrl();
            @endphp
            <div class="champion-profile">
                <div class="jersey-box">
                    <img src="{{ $champion->getJerseyUrl() }}" class="jersey-layer" height="80" width="75" alt="" />
                    <img src="{{ $left ?? '' }}" class="jersey-layer" height="80" width="75" style="display: {{ $left ? 'inline' : 'none' }} " alt="" />
                    <img src="{{ $right ?? '' }}" class="jersey-layer" height="80" width="75" style="display: {{ $right ? 'inline' : 'none' }} " alt=""  />
                    <img src="{{ $collar ?? '' }}" class="jersey-layer" height="80" width="75" style="display: {{ $collar ? 'inline' : 'none' }} " alt=""  />
                </div>
                <p>
                    <span>{{ $champion->name }}</span><br/>
                    <span>{{ strtoupper($champion->team) }}</span>
                </p>
            </div>
        @endforeach
        </div>

        <p>
            {{ $womenCount + $menCount }} champions: {{ $womenCount }} women, {{ $menCount }} men
        </p>
    </div>
</x-layout>