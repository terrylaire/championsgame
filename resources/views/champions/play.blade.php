<x-layout>
    <x-slot:additionalAssets>
        @vite([
            'resources/js/app.js'
        ])
    </x-slot:additionalAssets>
    <x-slot:nav>
        <div id="nav"><a href="{{ route('home') }}"><img src='/img/wholeurheader.png' alt="Wholeur" height="80" width="160"/></a></div>
    </x-slot>

    <div id="jersey-type" class="in-game">
        <div id="jersey-layers">
            <img src="{{ $baseJersey }}" id="game-jersey" class="jersey-layer" height="284" width="275" />
            <img src="{{ $leftJersey ?? '' }}" id="left-jersey" class="jersey-layer" height="284" width="275" style="display: {{ $leftJersey ? 'inline' : 'none' }} " />
            <img src="{{ $rightJersey ?? '' }}" id="right-jersey" class="jersey-layer" height="284" width="275" style="display: {{ $rightJersey ? 'inline' : 'none' }} "  />
            <img src="{{ $collarJersey ?? '' }}" id="collar-jersey" class="jersey-layer" height="284" width="275" style="display: {{ $collarJersey ? 'inline' : 'none' }} "  />
            <img src="{{ asset('img/loadingjersey.gif') }}" id="loading-jersey" class="jersey-layer" height="284" width="275" />
        </div>
        <div id="last-correction"><span id="last-correct-icon"></span> <span id="last-correct-rider"></span></div>
        <div id="over-message"></div>
        <div id="championship">
            <div id="championship-women" class="game-championship" style="display: {{ 'women' == $championship ? 'block' : 'none' }}" >
                <p id="championship-initial">{{ __('tr.championship-women-initial') }}</p>
                <p id="championship-complete">{{ __('tr.championship-women-jersey') }}</p>
            </div>
            <div id="championship-men" class="game-championship" style="display: {{ 'men' == $championship ? 'block' : 'none' }}">
                <p id="championship-initial">{{ __('tr.championship-men-initial') }}</p>
                <p id="championship-complete">{{ __('tr.championship-men-jersey') }}</p>
            </div>
        </div>
    </div>

    <div id="playing-zone">
        <form id="game-form" method="POST" action="{{ route('postAnswer') }}" onSubmit="return handleAnswer(this)" autocomplete="off">
            @csrf
            <div id="guess-grid">
                <div id="autocomplete">
                    <input id="i-answer" type="text" name="answer" placeholder="{{ __('tr.who-wears-it') }}" autofocus="true" required maxlength="42">
                </div>
                <input type="submit" class="submit-button" id="guess-grid-submit" value="bike"></input>
            </div>
        </form>
    </div>

    <div id="status-grid">
        <div id="correction"><span id="correct-icon"></span> <span id="correct-rider"></span></div>
        <div id="score">
                <span id="game-score">{{ $score }}</span><span id="game-goal">/{{ $goal }}</span>
        </div>
    </div>

    <div id="result-zone" style="display: none;">
        <div id="result-score">
            <span id="result-game-score">?</span>
        </div>
        <div id="star-rating">…</div>
        <div id="share-game">
            <p id="share-text">
                <a id="share-link" href="#" onclick="copyResult(this)">{{ __('tr.share') }}</a>
            </p>
        </div>
    </div>
    <div id="share-content" aria-hidden="true"></div>

    <div id="new-game-zone" style="display: none;">
        <div><a href="{{ route('retryGame') }}" id="retry-link" class="submit-button">{{ __('tr.retry') }}</a></div>
        <div><a href="{{ route('home') }}" id="home-link" class="submit-button">{{ __('tr.home') }}</a></div>
    </div>

    <div id="faq">
        <button class="faq-question">{{ __('tr.faq-question1') }}</button>
        <div class="faq-answer">
            {{ __('tr.faq-answer1') }}
        </div>
        <button class="faq-question">{{ __('tr.faq-question6') }}</button>
        <div class="faq-answer">
            {!! __('tr.faq-answer6') !!}
        </div>
        <button class="faq-question">{{ __('tr.faq-question2') }}</button>
        <div class="faq-answer">
            {{ __('tr.faq-answer2') }}
        </div>
        <button class="faq-question">{{ __('tr.faq-question3') }}</button>
        <div class="faq-answer">
            {{ __('tr.faq-answer3') }}
        </div>
        <button class="faq-question">{{ __('tr.faq-question4') }}</button>
        <div class="faq-answer">
            {{ __('tr.faq-answer4') }} <a href="https://gitlab.com/terrylaire/championsgame" title="Gitlab Wholeur">Gitlab</a>
        </div>
        <button class="faq-question">{{ __('tr.faq-question5') }}</button>
        <div class="faq-answer">
            {{ __('tr.faq-answer5') }} <a href="https://gitlab.com/terrylaire/championsgame" title="Gitlab Wholeur">Gitlab</a>
        </div>
    </div>

    <div id="footer">
        <p>{{ __('tr.footer-developer-before') }}<a href="https://terrylaire.fr" title="Terry Laire">Terry Laire</a>{{ __('tr.footer-developer-after') }}</p>
        <p style="font-size: smaller">Cycling icons created by <a href="https://www.flaticon.com/free-icons/cycling" title="cycling icons">Freepik - Flaticon</a>
        – Refresh icons created by <a href="https://www.flaticon.com/free-icons/refresh" title="refresh icons">Uniconlabs - Flaticon</a>
        – Home icons created by <a href="https://www.flaticon.com/free-icons/home" title="home icons">Freepik - Flaticon</a></p>
    </div>

    <div id="championship-id">{{ $championship }}</div>




    <script type="text/javascript">
        var allRiders = {!! $allSuggestionsJs !!};
        window.onload = function(ev) {
            autocomplete(document.getElementById("i-answer"), allRiders);
        }

        function handleAnswer(form) {
            answer = form.answer.value;
            if (!answer || (-1 == allRiders['men'].indexOf(answer) && -1 == allRiders['women'].indexOf(answer))) {
                form.answer.setCustomValidity("{{ __('tr.validation-unknown-champion') }}");
                form.answer.reportValidity();
                return false;
            }

            form.answer.setCustomValidity("");
            return checkAnswer(form);
        }
    </script>
</x-layout>