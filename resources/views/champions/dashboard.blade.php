<x-layout>
    <p>{{ $gamesCount }} played, {{ $endedGamesCount }} ended ({{ floor(($endedGamesCount / $gamesCount)*100) }} %)</p>
    <p>{{ $playersCount }} players</p>
    <div>
        @foreach ($days as $day => $games)
        <div class="stat-line">
            <span class="over" style="width: {{ $games[1] }}%;"></span><span class="started" style="width: {{ $games[0] }}%;"></span>
            {{ $games[0]+$games[1] ? $games[0]+$games[1] : '-' }}
        </div>
        @endforeach
    </div>
</x-layout>