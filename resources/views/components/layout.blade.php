<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <title>{{ __('tr.meta-title') }}</title>
        <meta name="keywords" content="{{ __('tr.meta-keywords') }}">
        <meta name="description" content="{{ __('tr.meta-description') }}">
        <meta name="author" content="Terry Laire">
        <meta name="viewport" content="width=device-width">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:creator" content="@terrylaire" />
        <meta property="og:title" content="{{ __('tr.meta-title') }}" />
        <meta property="og:description" content="{{ __('tr.meta-description') }}" />
        <meta property="og:image" content="https://wholeur.terrylaire.fr/img/wholeurlogo422.png" />
        <link rel="icon" type="image/jpeg" href="/img/favicon.jpg">
        @vite([
            'resources/css/app.css',
        ])
        {{ $additionalAssets ?? '' }}
    </head>
    <body>
        {{ $nav ?? '' }}
        <div id="content">
            {{ $slot }}
        </div>
    </body>
</html>