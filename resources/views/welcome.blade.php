<x-layout>
    @php
        $maxAll = 100; // Leave 100 if > 100
        $maxGender = 50; // Leave 50 if > 50
    @endphp
    <div id="logo">
        <img id="logo-img" src="/img/wholeurlogorect422.png" alt="Wholeur?" width="326" height="326" />
    </div>

    <div id="tagline">
        {{ __('tr.tagline') }}
    </div>

    <noscript><p>{{ __('tr.nojavascript') }}</p></noscript>

    @if (Route::isLocalized() || Route::isFallback())
        <ul id="lang">
            @foreach(LocaleConfig::getLocales() as $locale)
                @if ( ! App::isLocale($locale))
                    <li>
                        <a href="{{ Route::localizedUrl($locale) }}">
                            {{ strtoupper($locale) }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    @endif

    <form id="game-options" action="{{ route('initGame') }}" method="POST">
        @csrf
        <div id="options-championship" class="options-radio">
            <div>
                <input type="radio" name="championship" value="all" id="ch-all" checked="checked"><label for="ch-all" id="label-ch-all" class="label-championship">{{ __('tr.options-championship-both') }}</label>
            </div>
            <div>
                <input type="radio" name="championship" value="women" id="ch-women"><label for="ch-women" id="label-ch-women" class="label-championship">{{ __('tr.options-championship-women') }}</label>
            </div>
            <div>
                <input type="radio" name="championship" value="men" id="ch-men"><label for="ch-men" id="label-ch-men" class="label-championship">{{ __('tr.options-championship-men') }}</label>
            </div>
        </div>
        <div id="options-difficulty" class="options-radio">
            <div>
                <input type="radio" name="difficulty" value="easy" id="diff-easy" checked="checked"><label for="diff-easy" id="label-diff-easy" class="label-difficulty">×20</label>
            </div>
            <div>
                <input type="radio" name="difficulty" value="medium" id="diff-medium"><label for="diff-medium" id="label-diff-med" class="label-difficulty">×50</label>
            </div>
            <div>
                <input type="radio" name="difficulty" value="hard" id="diff-hard"><label for="diff-hard" id="label-diff-hard" class="label-difficulty">×{{ $maxAll }}</label>
            </div>
        </div>
        <div class="announce">
            {!! __('tr.announce') !!}
        </div>
        <input type="submit" id="options-submit" class="submit-button" value="Go" />
    </form>

    <div id="footer">
        <p>{{ __('tr.footer-developer-before') }}<a href="https://terrylaire.fr" title="Terry Laire">Terry Laire</a>{{ __('tr.footer-developer-after') }}</p>
        <p style="font-size: smaller">Cycling icons created by <a href="https://www.flaticon.com/free-icons/cycling" title="cycling icons">Freepik - Flaticon</a>&nbsp;– Riders icons created by <a href="https://www.flaticon.com/free-icons/riders" title="riders icons">Leremy - Flaticon</a></p>
    </div>

    <script type="text/javascript">
        var championshipSelEl = document.getElementById('options-championship');
        championshipSelEl.onchange = function (e) {
            var diffEasy = document.getElementById('label-diff-easy');
            var diffMed = document.getElementById('label-diff-med');
            var diffHard = document.getElementById('label-diff-hard');
            if ('all' == e.target.value) {
                diffEasy.innerHTML = '×20';
                diffMed.innerHTML = '×50';
                diffHard.innerHTML = '×{{ $maxAll }}';
            } else {
                diffEasy.innerHTML = '×10';
                diffMed.innerHTML = '×25';
                diffHard.innerHTML = '×{{ $maxGender }}';
            }
        }
    </script>
</x-layout>