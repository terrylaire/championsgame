<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ChampionController;
use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::localized(function () {
    Route::get('', function () {
        return view('welcome');
    })->name('home');

    Route::get('/play', [GameController::class, 'showGame'])->name('showGame');
    Route::post('/start', [GameController::class, 'initGame'])->name('initGame');
    Route::post('/champions/answer', [GameController::class, 'championsPost'])->name('postAnswer');
    Route::get('/every-national-champion', [ChampionController::class, 'listAll'])->name('listAllChampions');
    Route::get('/dash', [AdminController::class, 'dashboard'])->name('dashboard');
});

Route::get('/retry', [GameController::class, 'retryGame'])->name('retryGame');
Route::get('/start', function() {
    return redirect()->route('home');
});