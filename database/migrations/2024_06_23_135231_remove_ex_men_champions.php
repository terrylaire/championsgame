<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 0 where name like 'Alexey Lutsenko'");
        DB::update("update champions set `active` = 0 where name like 'Christine Majerus'");
        DB::update("update champions set `active` = 0 where name like 'Carina Schrempf'");
        DB::update("update champions set `active` = 0 where name like 'Gregor Mühlberger'");
        DB::update("update champions set `active` = 0 where name like 'Demi Vollering'");
        DB::update("update champions set `active` = 0 where name like 'Valentin Madouas'");
        DB::update("update champions set `active` = 0 where name like 'Susanne Andersen'");
        DB::update("update champions set `active` = 0 where name like 'Oier Lazkano'");
        DB::update("update champions set `active` = 0 where name like 'Jelena Erić'");
        DB::update("update champions set `active` = 0 where name like 'Itamar Einhorn'");
        DB::update("update champions set `active` = 0 where name like 'Quinn Simmons'");
        DB::update("update champions set `active` = 0 where name like 'Mathias Vacek'");
        DB::update("update champions set `active` = 0 where name like 'Nickolas Zukowsky'");
        DB::update("update champions set `active` = 0 where name like 'Dušan Rajović'");
        DB::update("update champions set `active` = 0 where name like 'Sébastien Reichenbach'");
        DB::update("update champions set `active` = 0 where name like 'Ben Healy'");
        DB::update("update champions set `active` = 0 where name like 'Ivo Oliveira'");
        DB::update("update champions set `active` = 0 where name like 'Rui Costa'");
        DB::update("update champions set `active` = 0 where name like 'Tadej Pogačar'");
        DB::update("update champions set `active` = 0 where name like 'Domen Novak'");
        DB::update("update champions set `active` = 0 where name like 'Remco Evenepoel'");
        DB::update("update champions set `active` = 0 where name like 'Marc Hirschi'");
        DB::update("update champions set `active` = 0 where name like 'Mattias Skjelmose'");
        DB::update("update champions set `active` = 0 where name like 'Simone Velasco'");
        DB::update("update champions set `active` = 0 where name like 'Mie Bjørndal Ottestad'");
        DB::update("update champions set `active` = 0 where name like 'Dylan van Baarle'");
        DB::update("update champions set `active` = 0 where name like 'Emanuel Buchmann'");
        DB::update("update champions set `active` = 0 where name like 'Fred Wright'");
        DB::update("update champions set `active` = 0 where name like 'Fredrik Dversnes'");
        DB::update("update champions set `active` = 0 where name like 'Alex Kirsch'");
        DB::update("update champions set `active` = 0 where name like 'Kevin Geniets'");
        DB::update("update champions set `active` = 0 where name like 'Lucas Eriksson'");
        DB::update("update champions set `active` = 0 where name like 'Chantal van den Broek-Blaak'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 1 where name like 'Alexey Lutsenko'");
        DB::update("update champions set `active` = 1 where name like 'Christine Majerus'");
        DB::update("update champions set `active` = 1 where name like 'Carina Schrempf'");
        DB::update("update champions set `active` = 1 where name like 'Gregor Mühlberger'");
        DB::update("update champions set `active` = 1 where name like 'Demi Vollering'");
        DB::update("update champions set `active` = 1 where name like 'Valentin Madouas'");
        DB::update("update champions set `active` = 1 where name like 'Susanne Andersen'");
        DB::update("update champions set `active` = 1 where name like 'Oier Lazkano'");
        DB::update("update champions set `active` = 1 where name like 'Jelena Erić'");
        DB::update("update champions set `active` = 1 where name like 'Itamar Einhorn'");
        DB::update("update champions set `active` = 1 where name like 'Quinn Simmons'");
        DB::update("update champions set `active` = 1 where name like 'Mathias Vacek'");
        DB::update("update champions set `active` = 1 where name like 'Nickolas Zukowsky'");
        DB::update("update champions set `active` = 1 where name like 'Dušan Rajović'");
        DB::update("update champions set `active` = 1 where name like 'Sébastien Reichenbach'");
        DB::update("update champions set `active` = 1 where name like 'Ben Healy'");
        DB::update("update champions set `active` = 1 where name like 'Ivo Oliveira'");
        DB::update("update champions set `active` = 1 where name like 'Rui Costa'");
        DB::update("update champions set `active` = 1 where name like 'Tadej Pogačar'");
        DB::update("update champions set `active` = 1 where name like 'Domen Novak'");
        DB::update("update champions set `active` = 1 where name like 'Remco Evenepoel'");
        DB::update("update champions set `active` = 1 where name like 'Marc Hirschi'");
        DB::update("update champions set `active` = 1 where name like 'Mattias Skjelmose'");
        DB::update("update champions set `active` = 1 where name like 'Simone Velasco'");
        DB::update("update champions set `active` = 1 where name like 'Mie Bjørndal Ottestad'");
        DB::update("update champions set `active` = 1 where name like 'Dylan van Baarle'");
        DB::update("update champions set `active` = 1 where name like 'Emanuel Buchmann'");
        DB::update("update champions set `active` = 1 where name like 'Fred Wright'");
        DB::update("update champions set `active` = 1 where name like 'Fredrik Dversnes'");
        DB::update("update champions set `active` = 1 where name like 'Alex Kirsch'");
        DB::update("update champions set `active` = 1 where name like 'Kevin Geniets'");
        DB::update("update champions set `active` = 1 where name like 'Lucas Eriksson'");
        DB::update("update champions set `active` = 1 where name like 'Chantal van den Broek-Blaak'");
    }
};
