<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 0 where `name` like 'Mischa Bredewold'");
        DB::update("update champions set `active` = 1, `team` = 'sdw', `ranking` = 4, `full` = 'euro', `left` = 'euro', `right` = 'euro', `collar` = NULL, `updated_at` = NOW() where `name` like 'Lorena Wiebes'");
        DB::update("update champions set `active` = 0 where `name` like 'Christophe Laporte'");
        DB::update("update champions set `active` = 1, `team` = 'soq', `ranking` = 73, `full` = 'euro', `left` = 'euro', `right` = 'euro', `collar` = NULL, `updated_at` = NOW() where `name` like 'Tim Merlier'");
        DB::update("update champions set `active` = 0 where `name` like 'Mathieu van der Poel'");
        DB::update("update champions set `active` = 1, `team` = 'uad', `ranking` = 1, `full` = 'world', `left` = 'world', `right` = 'world', `collar` = NULL, `updated_at` = NOW() where `name` like 'Tadej Pogačar'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 1 where `name` like 'Mischa Bredewold'");
        DB::update("update champions set `active` = 1, `team` = 'sdw', `ranking` = 4, `full` = NULL, `left` = 'euro', `right` = 'euro', `collar` = NULL, `updated_at` = NOW() where `name` like 'Lorena Wiebes'");
        DB::update("update champions set `active` = 1 where `name` like 'Christophe Laporte'");
        DB::update("update champions set `active` = 1, `team` = 'soq', `ranking` = 73, `full` = NULL, `left` = 'be', `right` = 'be', `collar` = NULL, `updated_at` = NOW() where `name` like 'Tim Merlier'");
        DB::update("update champions set `active` = 1 where `name` like 'Mathieu van der Poel'");
        DB::update("update champions set `active` = 0, `team` = 'uad', `ranking` = 1, `full` = 'si', `left` = 'si', `right` = 'si', `collar` = NULL, `updated_at` = NOW() where `name` like 'Tadej Pogačar'");
    }
};
