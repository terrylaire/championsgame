<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `full` = 'be', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Remco Evenepoel'");
        DB::update("update champions set `full` = 'world', `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Mathieu van der Poel'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `full` = 'world', `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Remco Evenepoel'");
        DB::update("update champions set `full` = NULL, `left` = 'nl', `right` = 'nl', `collar` = NULL, `updated_at` = NOW() where `name` like 'Mathieu van der Poel'");
    }
};
