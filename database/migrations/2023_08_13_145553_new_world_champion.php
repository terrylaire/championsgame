<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `full` = NULL, `left` = NULL, `right` = NULL, `collar` = NULL, `active` = 0, `updated_at` = NOW() where `name` like 'Annemiek van Vleuten'");
        DB::update("update champions set `full` = 'world', `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Lotte Kopecky'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `full` = 'world', `left` = 'world', `right` = 'world', `collar` = 'world', `active` = 1, `updated_at` = NOW() where `name` like 'Annemiek van Vleuten'");
        DB::update("update champions set `full` = 'be', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Lotte Kopecky'");
    }
};
