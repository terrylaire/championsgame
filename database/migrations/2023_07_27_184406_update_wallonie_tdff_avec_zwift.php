<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `full` = null, `left` = 'at-l', `right` = 'at-l', `collar` = NULL, `updated_at` = NOW() where `name` like 'Felix Grossschartner'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`) values ("Mathias Vacek", "men", "ltk", 182, "cz")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`) values ("Pfeiffer Georgi", "women", "dsm", 33, "gb")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `left`, `right`) values ("Marta Cavalli", "women", "fst", 10, "it", "it")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `left`, `right`) values ("Omer Goldstein", "men", "ipt", 504, "il", "il")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`) values ("Demi Vollering", "women", "sdw", 5, "nl")');
        DB::update("update champions set `active` = 1, `full` = 'be', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Lotte Kopecky'");
        DB::update("update champions set `active` = 1, `full` = 'ch', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Marlen Reusser'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 0, `full` = NULL, `left` = 'ch', `right` = 'ch', `collar` = 'ch', `updated_at` = NOW() where `name` like 'Marlen Reusser'");
        DB::update("update champions set `active` = 0, `full` = NULL, `left` = 'be', `right` = 'be', `collar` = 'be', `updated_at` = NOW() where `name` like 'Lotte Kopecky'");
        DB::delete('delete from champions where name like "Demi Vollering"');
        DB::delete('delete from champions where name like "Omer Goldstein"');
        DB::delete('delete from champions where name like "Marta Cavalli"');
        DB::delete('delete from champions where name like "Pfeiffer Georgi"');
        DB::delete('delete from champions where name like "Mathias Vacek"');
        DB::update("update champions set `active` = 0, `full` = 'at', `left` = 'at', `right` = 'at', `updated_at` = NOW() where `name` like 'Felix Grossschartner'");
    }
};
