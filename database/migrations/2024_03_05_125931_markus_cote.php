<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `team` = 'ipt', `ranking` = 247, `full` = 'panam', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Pier-André Coté'");
        DB::update("update champions set `active` = 1, `team` = 'tvl', `ranking` = 22, `full` = NULL, `left` = 'nl', `right` = 'nl', `collar` = 'nl', `updated_at` = NOW() where `name` like 'Riejanne Markus'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 0, `team` = 'hpm', `ranking` = 318, `full` = 'panam', `left` = NULL, `right` = 'ca', `collar` = NULL, `updated_at` = NOW() where `name` like 'Pier-André Coté'");
        DB::update("update champions set `active` = 0, `team` = 'tvl', `ranking` = 40, `full` = 'nl', `left` = 'nl', `right` = 'nl', `collar` = 'nl', `updated_at` = NOW() where `name` like 'Riejanne Markus'");
    }
};
