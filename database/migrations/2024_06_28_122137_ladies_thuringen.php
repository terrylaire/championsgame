<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 0, `updated_at` = NOW() where `name` like 'Alison Jackson'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Marie Schreiber", "women", "sdw", 2001, "lu", NULL, NULL, NULL)');
        DB::update("update champions set `active` = 1, `team` = 'fed', `ranking` = 28, `full` = NULL, `left` = 'at', `right` = 'at', `collar` = NULL, `updated_at` = NOW() where `name` like 'Christina Schweinberger'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 1, `updated_at` = NOW() where `name` like 'Alison Jackson'");
        DB::delete('delete from champions where name like "Marie Schreiber"');
        DB::update("update champions set `active` = 0, `team` = 'fed', `ranking` = 61, `full` = 'at', `left` = 'at', `right` = 'at', `collar` = 'at', `updated_at` = NOW() where `name` like 'Christina Schweinberger'");
    }
};
