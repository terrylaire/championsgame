<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `full` = null, `left` = 'ch', `right` = 'ch', `collar` = NULL, `updated_at` = NOW() where `name` like 'Robin Froidevaux'");
        DB::update("update champions set `active` = 1, `full` = null, `left` = 'es', `right` = 'es', `collar` = NULL, `updated_at` = NOW() where `name` like 'Carlos Rodríguez'");
        DB::update("update champions set `active` = 1, `full` = 'panam', `left` = NULL, `right` = 'ca', `collar` = NULL, `updated_at` = NOW() where `name` like 'Pier-André Coté'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Victoire Berteau", "women", "cof", 82, "fr", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Adam de Vos", "men", "hpm", 3608, NULL, NULL, "ca", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Kevin Geniets", "men", "gfc", 314, NULL, "lu", "lu", NULL)');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 0, `full` = 'ch', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Robin Froidevaux'");
        DB::update("update champions set `active` = 0, `full` = 'es', `left` = 'es', `right` = 'es', `collar` = NULL, `updated_at` = NOW() where `name` like 'Carlos Rodríguez'");
        DB::update("update champions set `active` = 0, `full` = 'ca', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Pier-André Coté'");
        DB::delete('delete from champions where name like "Victoire Berteau"');
        DB::delete('delete from champions where name like "Adam de Vos"');
        DB::delete('delete from champions where name like "Kevin Geniets"');
    }
};
