<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `team` = 'efc', `ranking` = 31, `full` = 'ca', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Alison Jackson'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Ben Healy", "men", "efe", 22, "ie", "ie", "ie", NULL)');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 1, `team` = 'tib', `ranking` = 64, `full` = NULL, `left` = 'ca', `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Alison Jackson'");
        DB::delete('delete from champions where name like "Ben Healy"');
    }
};
