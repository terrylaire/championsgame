<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Deactivate riders until we see their new jersey
        DB::update("update champions set `active` = 0 where name like 'Warren Barguil'");
        DB::update("update champions set `active` = 0 where name like 'Pascal Ackermann'");
        DB::update("update champions set `active` = 0 where name like 'Ryan Gibbons'");
        DB::update("update champions set `active` = 0 where name like 'George Bennett'");
        DB::update("update champions set `active` = 0 where name like 'Timo Roosen'");
        DB::update("update champions set `active` = 0 where name like 'Florian Sénéchal'");
        DB::update("update champions set `active` = 0 where name like 'Primož Roglič'");
        DB::update("update champions set `active` = 0 where name like 'Rui Costa'");
        DB::update("update champions set `active` = 0 where name like 'Davide Formolo'");
        DB::update("update champions set `active` = 0 where name like 'Rémi Cavagna'");
        DB::update("update champions set `active` = 0 where name like 'Dries De Bondt'");
        DB::update("update champions set `active` = 0 where name like 'Emilia Fahlin'");
        DB::update("update champions set `active` = 0 where name like 'Tobias Foss'");
        DB::update("update champions set `active` = 0 where name like 'Michael Valgren'");
        DB::update("update champions set `active` = 0 where name like 'Adam Ťoupalík'");
        DB::update("update champions set `active` = 0 where name like 'Pier-André Coté'");
        DB::update("update champions set `active` = 0 where name like 'Barnabás Peák'");
        DB::update("update champions set `active` = 0 where name like 'Luke Plapp'");
        DB::update("update champions set `active` = 0 where name like 'Nicole Frain'");
        DB::update("update champions set `active` = 0 where name like 'Miles Scotson'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 1 where name like 'Warren Barguil'");
        DB::update("update champions set `active` = 1 where name like 'Pascal Ackermann'");
        DB::update("update champions set `active` = 1 where name like 'Ryan Gibbons'");
        DB::update("update champions set `active` = 1 where name like 'George Bennett'");
        DB::update("update champions set `active` = 1 where name like 'Timo Roosen'");
        DB::update("update champions set `active` = 1 where name like 'Florian Sénéchal'");
        DB::update("update champions set `active` = 1 where name like 'Primož Roglič'");
        DB::update("update champions set `active` = 1 where name like 'Rui Costa'");
        DB::update("update champions set `active` = 1 where name like 'Davide Formolo'");
        DB::update("update champions set `active` = 1 where name like 'Rémi Cavagna'");
        DB::update("update champions set `active` = 1 where name like 'Dries De Bondt'");
        DB::update("update champions set `active` = 1 where name like 'Emilia Fahlin'");
        DB::update("update champions set `active` = 1 where name like 'Tobias Foss'");
        DB::update("update champions set `active` = 1 where name like 'Michael Valgren'");
        DB::update("update champions set `active` = 1 where name like 'Adam Ťoupalík'");
        DB::update("update champions set `active` = 1 where name like 'Pier-André Coté'");
        DB::update("update champions set `active` = 1 where name like 'Barnabás Peák'");
        DB::update("update champions set `active` = 1 where name like 'Luke Plapp'");
        DB::update("update champions set `active` = 1 where name like 'Nicole Frain'");
        DB::update("update champions set `active` = 1 where name like 'Miles Scotson'");
    }
};
