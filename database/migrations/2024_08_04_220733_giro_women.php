<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Juliette Labous", "women", "dfp", 11, "fr", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Urška Žigart", "women", "laj", 105, "si", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Kimberley Le Court Pienaar", "women", "ags", 260, "mu", "mu", "mu", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Franziska Koch", "women", "dfp", 150, "de", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Dominika Wlodarczyk", "women", "uad", 95, "pl", "pl-l", "pl-l", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Ella Wyllie", "women", "laj", 91, "nz", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Paula Patiño", "women", "mov", 140, "co", NULL, NULL, NULL)');
        DB::update("update champions set `active` = 1, `team` = 'sdw', `ranking` = 2001, `full` = 'nl', `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Chantal van den Broek-Blaak'");
        DB::update("update champions set `active` = 1, `team` = 'uxm', `ranking` = 115, `full` = 'no', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Mie Bjørndal Ottestad'");
        DB::update("update champions set `active` = 1, `updated_at` = NOW() where name like 'Jelena Erić'");

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Juliette Labous"');
        DB::delete('delete from champions where name like "Urška Žigart"');
        DB::delete('delete from champions where name like "Kimberley Le Court Pienaar"');
        DB::delete('delete from champions where name like "Franziska Koch"');
        DB::delete('delete from champions where name like "Dominika Wlodarczyk"');
        DB::delete('delete from champions where name like "Ella Wyllie"');
        DB::delete('delete from champions where name like "Paula Patiño"');
        DB::update("update champions set `active` = 0, `team` = 'sdw', `ranking` = 2001, `full` = NULL, `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Chantal van den Broek-Blaak'");
        DB::update("update champions set `active` = 0, `team` = 'uxm', `ranking` = 115, `full` = NULL, `left` = 'no', `right` = 'no', `collar` = NULL, `updated_at` = NOW() where `name` like 'Mie Bjørndal Ottestad'");
        DB::update("update champions set `active` = 0, `updated_at` = NOW() where name like 'Jelena Erić'");
    }
};
