<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    private $champions = [
        ['name' => 'Tim Merlier', 'championship' => 'men', 'team' => 'soq', 'ranking' => 27, 'full' => 'be'],
        ['name' => 'Wout Van Aert', 'championship' => 'men', 'team' => 'tjv', 'ranking' => 2, 'left' => 'be', 'right' => 'be', 'collar' => 'be'],
        ['name' => 'Dries De Bondt', 'championship' => 'men', 'team' => 'afc', 'ranking' => 101, 'left' => 'be', 'right' => 'be'],
        ['name' => 'Yves Lampaert', 'championship' => 'men', 'team' => 'soq', 'ranking' => 154, 'left' => 'be', 'right' => 'be'],
        ['name' => 'Oliver Naesen', 'championship' => 'men', 'team' => 'act', 'ranking' => 107, 'left' => 'be', 'right' => 'be'],
        ['name' => 'Florian Sénéchal', 'championship' => 'men', 'team' => 'soq', 'ranking' => 159, 'full' => 'fr'],
        ['name' => 'Rémi Cavagna', 'championship' => 'men', 'team' => 'soq', 'ranking' => 270, 'left' => 'fr', 'right' => 'fr'],
        ['name' => 'Arnaud Démare', 'championship' => 'men', 'team' => 'gfc', 'ranking' => 14, 'left' => 'fr', 'right' => 'fr'],
        ['name' => 'Warren Barguil', 'championship' => 'men', 'team' => 'ark', 'ranking' => 42, 'left' => 'fr', 'right' => 'fr', 'collar' => 'fr'],
        ['name' => 'Nacer Bouhanni', 'championship' => 'men', 'team' => 'ark', 'ranking' => 97, 'left' => 'fr', 'right' => 'fr'],
        ['name' => 'Carlos Rodríguez', 'championship' => 'men', 'team' => 'igd', 'ranking' => 29, 'full' => 'es', 'left' => 'es', 'right' => 'es'],
        ['name' => 'Omar Fraile', 'championship' => 'men', 'team' => 'igd', 'ranking' => 419, 'left' => 'es', 'right' => 'es'],
        ['name' => 'Luis León Sánchez', 'championship' => 'men', 'team' => 'ast', 'ranking' => 264, 'left' => 'es-l', 'right' => 'es-l'],
        ['name' => 'Jesús Herrada', 'championship' => 'men', 'team' => 'cof', 'ranking' => 65, 'left' => 'es', 'right' => 'es', 'collar' => 'es'],
        ['name' => 'Ion Izagirre', 'championship' => 'men', 'team' => 'cof', 'ranking' => 79, 'left' => 'es', 'right' => 'es'],
        ['name' => 'Pascal Eenkhoorn', 'championship' => 'men', 'team' => 'ltd', 'ranking' => 450, 'full' => 'nl', 'collar' => 'nl'],
        ['name' => 'Timo Roosen', 'championship' => 'men', 'team' => 'tjv', 'ranking' => 526, 'left' => 'nl', 'right' => 'nl', 'collar' => 'nl'],
        ['name' => 'Mathieu van der Poel', 'championship' => 'men', 'team' => 'afc', 'ranking' => 9, 'left' => 'nl', 'right' => 'nl'],
        ['name' => 'Ramon Sinkeldam', 'championship' => 'men', 'team' => 'afc', 'ranking' => 667, 'left' => 'nl', 'right' => 'nl'],
        ['name' => 'Ben Swift', 'championship' => 'men', 'team' => 'igd', 'ranking' => 917, 'left' => 'gb2', 'right' => 'gb2'],
        ['name' => 'Connor Swift', 'championship' => 'men', 'team' => 'igd', 'ranking' => 225, 'left' => 'gb2', 'right' => 'gb2'],
        ['name' => 'Geraint Thomas', 'championship' => 'men', 'team' => 'igd', 'ranking' => 31, 'left' => 'gb2', 'right' => 'gb2'],
        ['name' => 'Filippo Zana', 'championship' => 'men', 'team' => 'jay', 'ranking' => 170, 'full' => 'it', 'left' => 'it', 'right' => 'it', 'collar' => 'it'],
        ['name' => 'Davide Formolo', 'championship' => 'men', 'team' => 'uad', 'ranking' => 242, 'left' => 'it', 'right' => 'it'],
        ['name' => 'Luke Plapp', 'championship' => 'men', 'team' => 'igd', 'ranking' => 151, 'full' => 'au', 'left' => 'au', 'right' => 'au'],
        ['name' => 'Miles Scotson', 'championship' => 'men', 'team' => 'gfc', 'ranking' => 1378, 'left' => 'au', 'right' => 'au'],
        ['name' => 'Alexander Kamp', 'championship' => 'men', 'team' => 'tud', 'ranking' => 104, 'full' => 'dk'],
        ['name' => 'Kasper Asgreen', 'championship' => 'men', 'team' => 'soq', 'ranking' => 143, 'left' => 'dk', 'right' => 'dk'],
        ['name' => 'Michael Mørkøv', 'championship' => 'men', 'team' => 'soq', 'ranking' => 523, 'left' => 'dk', 'right' => 'dk'],
        ['name' => 'Michael Valgren', 'championship' => 'men', 'team' => 'efd', 'ranking' => 385, 'left' => 'dk'],
        ['name' => 'Kristijan Koren', 'championship' => 'men', 'team' => 'adr', 'ranking' => 588, 'full' => 'si', 'left' => 'si', 'right' => 'si'],
        ['name' => 'Primož Roglič', 'championship' => 'men', 'team' => 'tjv', 'ranking' => 24, 'left' => 'si', 'right' => 'si', 'collar' => 'si'],
        ['name' => 'Domen Novak', 'championship' => 'men', 'team' => 'uad', 'ranking' => 306, 'left' => 'si', 'right' => 'si'],
        ['name' => 'Jan Tratnik', 'championship' => 'men', 'team' => 'tjv', 'ranking' => 145, 'left' => 'si', 'right' => 'si', 'collar' => 'si'],
        ['name' => 'Esteban Chaves', 'championship' => 'men', 'team' => 'efe', 'ranking' => 100, 'full' => 'co', 'left' => 'co', 'right' => 'co'],
        ['name' => 'Larry Warbasse', 'championship' => 'men', 'team' => 'act', 'ranking' => 1982, 'left' => 'us', 'right' => 'us'],
        ['name' => 'Nils Politt', 'championship' => 'men', 'team' => 'boh', 'ranking' => 156, 'full' => 'de', 'left' => 'de', 'right' => 'de', 'collar' => 'de'],
        ['name' => 'Pascal Ackermann', 'championship' => 'men', 'team' => 'uad', 'ranking' => 171, 'left' => 'de', 'right' => 'de'],
        ['name' => 'Rasmus Tiller', 'championship' => 'men', 'team' => 'uxt', 'ranking' => 116, 'full' => 'no'],
        ['name' => 'Tobias Foss', 'championship' => 'men', 'team' => 'tjv', 'ranking' => 112, 'left' => 'no', 'right' => 'no', 'collar' => 'no'],
        ['name' => 'Sven Erik Bystrøm', 'championship' => 'men', 'team' => 'icw', 'ranking' => 573, 'left' => 'no', 'right' => 'no'],
        ['name' => 'Vegard Stake Laengen', 'championship' => 'men', 'team' => 'uad', 'ranking' => 1636, 'left' => 'no', 'right' => 'no'],
        ['name' => 'Edvald Boasson Hagen', 'championship' => 'men', 'team' => 'ten', 'ranking' => 250, 'right' => 'no'],
        ['name' => 'Robin Froidevaux', 'championship' => 'men', 'team' => 'tud', 'ranking' => 433, 'full' => 'ch'],
        ['name' => 'Silvan Dillier', 'championship' => 'men', 'team' => 'adc', 'ranking' => 1052, 'left' => 'ch', 'right' => 'ch'],
        ['name' => 'Stefan Küng', 'championship' => 'men', 'team' => 'gfc', 'ranking' => 7, 'left' => 'ch', 'right' => 'ch'],
        ['name' => 'Sébastien Reichenbach', 'championship' => 'men', 'team' => 'tud', 'ranking' => 178, 'left' => 'ch-xxl'],
        ['name' => 'Michael Schär', 'championship' => 'men', 'team' => 'act', 'ranking' => 847, 'left' => 'ch', 'right' => 'ch'],
        ['name' => 'João Almeida', 'championship' => 'men', 'team' => 'uad', 'ranking' => 40, 'full' => 'pt', 'left' => 'pt', 'right' => 'pt'],
        ['name' => 'George Bennett', 'championship' => 'men', 'team' => 'uad', 'ranking' => 204, 'left' => 'nz', 'right' => 'nz'],
        ['name' => 'Michael Vink', 'championship' => 'men', 'team' => 'uad', 'ranking' => 1417, 'left' => 'nz', 'right' => 'nz'],
        ['name' => 'Felix Grossschartner', 'championship' => 'men', 'team' => 'uad', 'ranking' => 203, 'full' => 'at', 'left' => 'at', 'right' => 'at'],
        ['name' => 'Merhawi Kudus', 'championship' => 'men', 'team' => 'efe', 'ranking' => 453, 'full' => 'er'],
        ['name' => 'Pier-André Coté', 'championship' => 'men', 'team' => 'hpm', 'ranking' => 318, 'full' => 'ca'],
        ['name' => 'Matěj Zahálka', 'championship' => 'men', 'team' => 'eka', 'ranking' => 430, 'full' => 'cz', 'left' => 'cz', 'right' => 'cz', 'collar' => 'cz'],
        ['name' => 'Adam Ťoupalík', 'championship' => 'men', 'team' => 'eka', 'ranking' => 364, 'left' => 'cz', 'right' => 'cz'],
        ['name' => 'Josef Černý', 'championship' => 'men', 'team' => 'soq', 'ranking' => 427, 'left' => 'cz', 'right' => 'cz'],
        ['name' => 'Jan Bárta', 'championship' => 'men', 'team' => 'eka', 'ranking' => 471, 'left' => 'cz', 'right' => 'cz', 'collar' => 'cz'],
        ['name' => 'Louis Meintjes', 'championship' => 'men', 'team' => 'icw', 'ranking' => 53, 'left' => 'za', 'right' => 'za'],
        ['name' => 'Attila Valter', 'championship' => 'men', 'team' => 'tjv', 'ranking' => 127, 'full' => 'hu', 'left' => 'hu', 'right' => 'hu', 'collar' => 'hu'],
        ['name' => 'Orluis Aular', 'championship' => 'men', 'team' => 'cjr', 'ranking' => 164, 'full' => 've'],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->champions as $champion) {
            DB::insert("insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("
                . "'" . $champion['name'] . "', "
                . "'" . $champion['championship'] . "', "
                . "'" . $champion['team'] . "', "
                . "'" . $champion['ranking'] . "', "
                . "'" . ($champion['full'] ?? NULL) . "', "
                . "'" . ($champion['left'] ?? NULL) . "', "
                . "'" . ($champion['right'] ?? NULL) . "', "
                . "'" . ($champion['collar'] ?? NULL)
                . "')");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->champions as $champion) {
            DB::delete('delete from champions where `name` like ?', [$champion['name']]);
        }
    }
};
