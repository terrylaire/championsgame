<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `right`) values ("Giacomo Nizzolo", "men", "ipt", 93, "it")');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Giacomo Nizzolo"');
    }
};
