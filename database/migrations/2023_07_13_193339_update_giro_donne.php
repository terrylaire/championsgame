<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`) values ("Ana Vitória Magalhães", "women", "bdu", 1001, "br", "br", "br")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`) values ("Carina Schrempf", "women", "fed", 1001, "at", "at", "at")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`) values ("Ally Wollaston", "women", "ags", 235, "nz")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`) values ("Susanne Andersen", "women", "uxt", 1001, "no")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`) values ("Eri Yonamine", "women", "hpw", 155, "jp", "jp", "jp")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`) values ("Elisa Longo Borghini", "women", "ltk", 2, "it")');
        DB::update("update champions set `active` = 1, `full` = null, `left` = 'dk', `right` = 'dk', `updated_at` = NOW() where `name` like 'Cecilie Uttrup Ludwig'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`) values ("Thi That Nguyen", "women", "cgs", 1001, "asia")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`) values ("Chloe Dygert", "women", "csr", 1001, "us")');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Chloe Dygert"');
        DB::delete('delete from champions where name like "Thi That Nguyen"');
        DB::update("update champions set `active` = 0, `full` = 'dk', `left` = NULL, `right` = NULL, `updated_at` = NOW() where `name` like 'Cecilie Uttrup Ludwig'");
        DB::delete('delete from champions where name like "Elisa Longo Borghini"');
        DB::delete('delete from champions where name like "Eri Yonamine"');
        DB::delete('delete from champions where name like "Susanne Andersen"');
        DB::delete('delete from champions where name like "Ally Wollaston"');
        DB::delete('delete from champions where name like "Carina Schrempf"');
        DB::delete('delete from champions where name like "Ana Vitória Magalhães"');
    }
};
