<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `team` = 'q36', `ranking` = 230, `full` = NULL, `left` = 'ie', `right` = 'ie', `collar` = NULL, `updated_at` = NOW() where `name` like 'Rory Townsend'");
        DB::update("update champions set `active` = 1, `team` = 'ags', `ranking` = 79, `full` = 'nz', `left` = 'nz2', `right` = 'nz2', `collar` = NULL, `updated_at` = NOW() where `name` like 'Ally Wollaston'");
        DB::update("update champions set `active` = 1, `team` = 'uad', `ranking` = 111, `full` = NULL, `left` = 'si', `right` = 'si', `collar` = NULL, `updated_at` = NOW() where `name` like 'Eugenia Bujak'");
        DB::update("update champions set `active` = 1, `team` = 'hph', `ranking` = 47, `full` = NULL, `left` = 'fr', `right` = 'fr', `collar` = NULL, `updated_at` = NOW() where `name` like 'Audrey Cordon-Ragot'");
        DB::update("update champions set `active` = 1, `team` = 'efc', `ranking` = 257, `full` = NULL, `left` = 'us2', `right` = 'us2', `collar` = NULL, `updated_at` = NOW() where `name` like 'Coryn Labecki'");
        DB::update("update champions set `active` = 0 where `name` like 'Brodie Chapman'");
        DB::update("update champions set `active` = 0 where `name` like 'Josie Talbot'");
        DB::update("update champions set `active` = 0 where `name` like 'Sarah Roy'");
        DB::update("update champions set `active` = 0 where `name` like 'Antri Christoforou'");
        DB::update("update champions set `active` = 0 where `name` like 'Eri Yonamine'");
        DB::update("update champions set `active` = 0 where `name` like 'Ana Vitória Magalhães'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Negasi Haylu Abreha", "men", "q36", 1439, NULL, "et", "et", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Sarah Gigante", "women", "ags", 2001, NULL, "au", NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Ruby Roseman-Gannon", "women", "laj", 29, "au", "au", "au", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Safia al Sayegh", "women", "uad", 204, "ae", "ae", "ae", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Anastasia Carbonari", "women", "uad", 287, "lv", "lv", "lv", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Sophie Edwards", "women", "ara", 118, "ocea", "ocea", "ocea", "ocea")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Ruth Edwards", "women", "hph", 2001, NULL, "us3", "us3", NULL)');
        DB::update("update champions set `team` = 'laj' where `team` like 'liv'");
        DB::update("update champions set `team` = 'hph' where `team` like 'hpw'");

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 0, `team` = 'beb', `ranking` = 481, `full` = 'ie', `left` = 'ie', `right` = 'ie', `collar` = NULL, `updated_at` = NOW() where `name` like 'Rory Townsend'");
        DB::update("update champions set `active` = 1, `team` = 'ags', `ranking` = 79, `full` = 'nz', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Ally Wollaston'");
        DB::update("update champions set `active` = 1, `team` = 'uad', `ranking` = 137, `full` = 'si', `left` = 'si', `right` = 'si', `collar` = NULL, `updated_at` = NOW() where `name` like 'Eugenia Bujak'");
        DB::update("update champions set `active` = 0, `team` = 'hpw', `ranking` = 25, `full` = 'fr', `left` = 'fr', `right` = 'fr', `collar` = 'fr', `updated_at` = NOW() where `name` like 'Audrey Cordon-Ragot'");
        DB::update("update champions set `active` = 1, `team` = 'tvl', `ranking` = 257, `full` = NULL, `left` = 'us-l', `right` = 'us-l', `collar` = 'us-l', `updated_at` = NOW() where `name` like 'Coryn Labecki'");
        DB::update("update champions set `active` = 1 where `name` like 'Brodie Chapman'");
        DB::update("update champions set `active` = 1 where `name` like 'Josie Talbot'");
        DB::update("update champions set `active` = 1 where `name` like 'Sarah Roy'");
        DB::update("update champions set `active` = 1 where `name` like 'Antri Christoforou'");
        DB::update("update champions set `active` = 1 where `name` like 'Eri Yonamine'");
        DB::update("update champions set `active` = 1 where `name` like 'Ana Vitória Magalhães'");
        DB::delete('delete from champions where name like "Negasi Haylu Abreha"');
        DB::delete('delete from champions where name like "Sarah Gigante"');
        DB::delete('delete from champions where name like "Ruby Roseman-Gannon"');
        DB::delete('delete from champions where name like "Safia al Sayegh"');
        DB::delete('delete from champions where name like "Anastasia Carbonari"');
        DB::delete('delete from champions where name like "Sophie Edwards"');
        DB::delete('delete from champions where name like "Ruth Edwards"');
        DB::update("update champions set `team` = 'liv' where `team` like 'laj'");
        DB::update("update champions set `team` = 'hpw' where `team` like 'hph'");
    }
};
