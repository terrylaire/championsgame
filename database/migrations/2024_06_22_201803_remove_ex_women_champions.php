<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 0 where name like 'Marlen Reusser'");
        DB::update("update champions set `active` = 0 where name like 'Liane Lippert'");
        DB::update("update champions set `active` = 0 where name like 'Chloe Dygert'");
        DB::update("update champions set `active` = 0 where name like 'Mavi García'");
        DB::update("update champions set `active` = 0 where name like 'Victoire Berteau'");
        DB::update("update champions set `active` = 0 where name like 'Emilia Fahlin'");
        DB::update("update champions set `active` = 0 where name like 'Luyao Zeng'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 1 where name like 'Marlen Reusser'");
        DB::update("update champions set `active` = 1 where name like 'Liane Lippert'");
        DB::update("update champions set `active` = 1 where name like 'Chloe Dygert'");
        DB::update("update champions set `active` = 1 where name like 'Mavi García'");
        DB::update("update champions set `active` = 1 where name like 'Victoire Berteau'");
        DB::update("update champions set `active` = 1 where name like 'Emilia Fahlin'");
        DB::update("update champions set `active` = 1 where name like 'Luyao Zeng'");
    }
};
