<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    private $stillChampions = [
        "Annemiek van Vleuten",
        "Lorena Wiebes",
        "Josie Talbot",
        "Brodie Chapman",
        "Liane Lippert",
        "Mavi García",
        "Diana Peñuela",
        "Anniina Ahtosalo",
        "Antri Christoforou",
        "Blanka Vas",
        "Christine Majerus",
    ];

    private $championsAgain = [
        "Marlen Reusser",
        "Lotte Kopecky",
    ];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Deactivate every 2022 champions
        DB::update('update champions set active = 0 where championship = \'women\' and full <> \'\'');

        // Deactivate previous champions that won in 2023
        foreach ($this->championsAgain as $champion) {
            DB::update('update champions set active = 0 where name = \'' . $champion . '\'');
        }

        // Reactivate every champion still champion in 2023
        foreach ($this->stillChampions as $champion) {
            DB::update('update champions set active = 1 where name = \'' . $champion . '\'');
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {

        // Deactivate every champion still champion in 2023
        foreach ($this->stillChampions as $champion) {
            DB::update('update champions set active = 0 where name = \'' . $champion . '\'');
        }

        // Reactivate previous champions that won in 2023
        foreach ($this->championsAgain as $champion) {
            DB::update('update champions set active = 1 where name = \'' . $champion . '\'');
        }

        // Reactivate every 2022 champions
        DB::update('update champions set active = 1 where championship = \'women\' and full <> \'\'');
    }
};
