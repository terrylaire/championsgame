<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `full` = null where `full` = ''");
        DB::update("update champions set `left` = null where `left` = ''");
        DB::update("update champions set `right` = null where `right` = ''");
        DB::update("update champions set `collar` = null where `collar` = ''");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `full` = '' where `full` is null");
        DB::update("update champions set `left` = '' where `left` is null");
        DB::update("update champions set `right` = '' where `right` is null");
        DB::update("update champions set `collar` = '' where `collar` is null");
    }
};
