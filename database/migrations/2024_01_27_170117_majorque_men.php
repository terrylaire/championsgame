<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 0 where name like 'Jonathan Klever Caicedo'");
        DB::update("update champions set `active` = 1, `team` = 'uad', `ranking` = 156, `full` = NULL, `left` = 'de', `right` = 'de', `collar` = NULL, `updated_at` = NOW() where `name` like 'Nils Politt'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Georgios Bouglas", "men", "bbh", 384, "gr", NULL, NULL, "gr")');
        DB::update("update champions set `active` = 1, `team` = 'efe', `ranking` = 38, `full` = NULL, `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Rui Costa'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 1 where name like 'Jonathan Klever Caicedo'");
        DB::update("update champions set `active` = 0, `team` = 'boh', `ranking` = 156, `full` = 'de', `left` = 'de', `right` = 'de', `collar` = 'de', `updated_at` = NOW() where `name` like 'Nils Politt'");
        DB::delete('delete from champions where name like "Georgios Bouglas"');
        DB::update("update champions set `active` = 0, `team` = 'iwa', `ranking` = 300, `full` = NULL, `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Rui Costa'");
    }
};
