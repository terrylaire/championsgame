<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `left`, `right`) values ("Rafał Majka", "men", "uad", 215, "pl-l", "pl-l")');
        DB::update("update champions set `active` = 1, `full` = null, `left` = 'nl', `right` = 'nl', `collar` = NULL, `updated_at` = NOW() where `name` like 'Pascal Eenkhoorn'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `right`) values ("Barnabás Peák", "men", "hpm", 286, "hu")');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Barnabás Peák"');
        DB::update("update champions set `active` = 0, `full` = 'nl', `left` = NULL, `right` = NULL, `collar` = 'nl', `updated_at` = NOW() where `name` like 'Pascal Eenkhoorn'");
        DB::delete('delete from champions where name like "Rafał Majka"');
    }
};
