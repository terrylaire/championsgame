<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique('champions_name_unique_index'); // Champion's name
            $table->string('championship', 6); // men or women
            $table->string('team', 6); // 3-chars current team code
            $table->integer('ranking'); // Difficulty ranking
            $table->string('full', 6)->nullable(); // Code of full jersey pattern
            $table->string('left', 6)->nullable(); // Code of left hand pattern
            $table->string('right', 6)->nullable(); // Code of right hand pattern
            $table->string('collar', 6)->nullable(); // Code of collar pattern
            $table->boolean('active')->default(true); // Active in the game
            $table->boolean('duplicate')->default(false); // Has a duplicate (same team and patterns)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champions');
    }
};
