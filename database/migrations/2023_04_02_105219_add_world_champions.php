<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{

    private $champions = [
        ['name' => 'Annemiek van Vleuten', 'championship' => 'women', 'team' => 'mov', 'ranking' => 1, 'full' => 'world', 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Remco Evenepoel', 'championship' => 'men', 'team' => 'soq', 'ranking' => 3, 'full' => 'world', 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Elisa Balsamo', 'championship' => 'women', 'team' => 'tfs', 'ranking' => 6, 'full' => 'it', 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Julian Alaphilippe', 'championship' => 'men', 'team' => 'soq', 'ranking' => 105, 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Mads Pedersen', 'championship' => 'men', 'team' => 'tfs', 'ranking' => 20, 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Chantal van den Broek-Blaak', 'championship' => 'women', 'team' => 'sdw', 'ranking' => 45, 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Peter Sagan', 'championship' => 'men', 'team' => 'ten', 'ranking' => 152, 'full' => 'sk', 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Amalie Dideriksen', 'championship' => 'women', 'team' => 'uxt', 'ranking' => 143, 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Elizabeth Deignan', 'championship' => 'women', 'team' => 'tfs', 'ranking' => 1000, 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Michał Kwiatkowski', 'championship' => 'men', 'team' => 'igd', 'ranking' => 142, 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Marianne Vos', 'championship' => 'women', 'team' => 'jvw', 'ranking' => 24, 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Rui Costa', 'championship' => 'men', 'team' => 'icw', 'ranking' => 300, 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Mark Cavendish', 'championship' => 'men', 'team' => 'ast', 'ranking' => 89, 'full' => 'gb', 'left' => 'world', 'right' => 'world', 'collar' => 'world'],
        ['name' => 'Lorena Wiebes', 'championship' => 'women', 'team' => 'sdw', 'ranking' => 2, 'full' => 'euro', 'left' => 'euro', 'right' => 'euro'],
        ['name' => 'Fabio Jakobsen', 'championship' => 'men', 'team' => 'soq', 'ranking' => 47, 'full' => 'euro', 'left' => 'euro', 'right' => 'euro', 'collar' => 'euro'],
        ['name' => 'Ellen van Dijk', 'championship' => 'women', 'team' => 'tfs', 'ranking' => 20, 'left' => 'euro', 'right' => 'euro', 'collar' => 'euro'],
        ['name' => 'Matteo Trentin', 'championship' => 'men', 'team' => 'uad', 'ranking' => 45, 'left' => 'euro', 'right' => 'euro'],
        ['name' => 'Alexander Kristoff', 'championship' => 'men', 'team' => 'uxt', 'ranking' => 8, 'left' => 'euro', 'right' => 'euro'],
        ['name' => 'Elia Viviani', 'championship' => 'men', 'team' => 'igd', 'ranking' => 311, 'left' => 'euro', 'right' => 'euro'],
        ['name' => 'Josie Talbot', 'championship' => 'women', 'team' => 'cof', 'ranking' => 105, 'full' => 'ocea', 'left' => 'ocea', 'right' => 'ocea', 'collar' => 'ocea'],
        ['name' => 'Richard Carapaz', 'championship' => 'men', 'team' => 'efe', 'ranking' => 21, 'full' => 'ec', 'left' => 'olym', 'right' => 'olym'],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->champions as $champion) {
            DB::insert("insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("
                . "'" . $champion['name'] . "', "
                . "'" . $champion['championship'] . "', "
                . "'" . $champion['team'] . "', "
                . "'" . $champion['ranking'] . "', "
                . "'" . ($champion['full'] ?? NULL) . "', "
                . "'" . ($champion['left'] ?? NULL) . "', "
                . "'" . ($champion['right'] ?? NULL) . "', "
                . "'" . ($champion['collar'] ?? NULL)
                . "')");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->champions as $champion) {
            DB::delete('delete from champions where `name` like ?', [$champion['name']]);
        }
    }
};
