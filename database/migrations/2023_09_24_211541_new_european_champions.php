<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `full` = NULL, `left` = NULL, `right` = NULL, `collar` = NULL, `active` = 0, `updated_at` = NOW() where `name` like 'Lorena Wiebes'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `created_at`, `updated_at`) values ("Mischa Bredewold", "women", "sdw", 56, "euro", NOW(), NOW())');
        DB::update("update champions set `full` = NULL, `left` = NULL, `right` = NULL, `collar` = NULL, `active` = 0, `updated_at` = NOW() where `name` like 'Fabio Jakobsen'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `created_at`, `updated_at`) values ("Christophe Laporte", "men", "tjv", 13, "euro", NOW(), NOW())');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `full` = 'euro', `left` = 'euro', `right` = 'euro', `collar` = NULL, `active` = 1, `updated_at` = NOW() where `name` like 'Lorena Wiebes'");
        DB::delete('delete from champions where name like "Mischa Bredewold"');
        DB::update("update champions set `full` = 'euro', `left` = 'euro', `right` = 'euro', `collar` = 'euro', `active` = 1, `updated_at` = NOW() where `name` like 'Fabio Jakobsen'");
        DB::delete('delete from champions where name like "Christophe Laporte"');
    }
};
