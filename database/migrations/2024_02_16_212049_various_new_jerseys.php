<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `team` = 'sdw', `ranking` = 4, `full` = NULL, `left` = 'euro', `right` = 'euro', `collar` = NULL, `updated_at` = NOW() where `name` like 'Lorena Wiebes'");
        DB::update("update champions set `active` = 1, `team` = 'gfc', `ranking` = 243, `full` = NULL, `left` = 'no', `right` = 'no', `collar` = NULL, `updated_at` = NOW() where `name` like 'Sven Erik Bystrøm'");
        DB::update("update champions set `active` = 1, `team` = 'efe', `ranking` = 599, `full` = NULL, `left` = 'dk', `right` = 'dk', `collar` = NULL, `updated_at` = NOW() where `name` like 'Michael Valgren'");
        DB::update("update champions set `active` = 0 where name like 'Ally Wollaston'");
        DB::update("update champions set `active` = 0 where name like 'Esteban Chaves'");
        DB::update("update champions set `active` = 0 where name like 'Richard Carapaz'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 0, `team` = 'sdw', `ranking` = 2, `full` = NULL, `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Lorena Wiebes'");
        DB::update("update champions set `active` = 0, `team` = 'iwa', `ranking` = 243, `full` = NULL, `left` = 'no', `right` = 'no', `collar` = NULL, `updated_at` = NOW() where `name` like 'Sven Erik Bystrøm'");
        DB::update("update champions set `active` = 0, `team` = 'efd', `ranking` = 385, `full` = NULL, `left` = 'dk', `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Michael Valgren'");
        DB::update("update champions set `active` = 1 where name like 'Ally Wollaston'");
        DB::update("update champions set `active` = 1 where name like 'Esteban Chaves'");
        DB::update("update champions set `active` = 1 where name like 'Richard Carapaz'");
    }
};
