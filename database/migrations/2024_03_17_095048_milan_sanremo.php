<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `team` = 'boh', `ranking` = 4, `full` = NULL, `left` = 'si', `right` = 'si', `collar` = NULL, `updated_at` = NOW() where `name` like 'Primož Roglič'");
        DB::update("update champions set `active` = 1, `team` = 'tud', `ranking` = 134, `full` = NULL, `left` = 'dk', `right` = 'dk', `collar` = NULL, `updated_at` = NOW() where `name` like 'Alexander Kamp'");
        DB::update("update champions set `active` = 1, `team` = 'dat', `ranking` = 391, `full` = NULL, `left` = 'no', `right` = 'no', `collar` = NULL, `updated_at` = NOW() where `name` like 'Edvald Boasson Hagen'");
        DB::update("update champions set `active` = 1, `team` = 'igd', `ranking` = 147, `full` = 'ec', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Jhonatan Narváez'");
        DB::update("update champions set `active` = 0 where `name` like 'Ahmed Madan'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 0, `team` = 'tvl', `ranking` = 24, `full` = NULL, `left` = 'si', `right` = 'si', `collar` = 'si', `updated_at` = NOW() where `name` like 'Primož Roglič'");
        DB::update("update champions set `active` = 0, `team` = 'tud', `ranking` = 104, `full` = 'dk', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Alexander Kamp'");
        DB::update("update champions set `active` = 0, `team` = 'ten', `ranking` = 391, `full` = NULL, `left` = NULL, `right` = 'no', `collar` = NULL, `updated_at` = NOW() where `name` like 'Edvald Boasson Hagen'");
        DB::update("update champions set `active` = 1, `team` = 'igd', `ranking` = 147, `full` = NULL, `left` = 'ec', `right` = 'ec', `collar` = NULL, `updated_at` = NOW() where `name` like 'Jhonatan Narváez'");
        DB::update("update champions set `active` = 1 where `name` like 'Ahmed Madan'");
    }
};
