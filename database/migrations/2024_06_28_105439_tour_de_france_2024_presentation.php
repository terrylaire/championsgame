<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Sean Quinn", "men", "efe", 562, "us", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Alberto Bettiol", "men", "efe", 161, "it", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Arnaud de Lie", "men", "ltd", 14, "be", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Dylan Groenewegen", "men", "jay", 56, "nl", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Paul Lapeira", "men", "gfc", 595, "fr", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Alex Aranburu", "men", "mov", 43, "es", "es", "es", "es")');
        DB::update("update champions set `active` = 1, `team` = 'efe', `ranking` = 38, `full` = 'pt', `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Rui Costa'");
        DB::update("update champions set `active` = 1, `full` = NULL, `left` = 'world', `right` = 'world', `collar` = NULL, `updated_at` = NOW() where `name` like 'Remco Evenepoel'");
        DB::update("update champions set `active` = 1, `full` = NULL, `left` = 'fr', `right` = 'fr', `collar` = NULL, `updated_at` = NOW() where `name` like 'Valentin Madouas'");
        DB::update("update champions set `active` = 1, `ranking` = 368, `full` = 'lu', `left` = 'lu', `right` = 'lu', `collar` = NULL, `updated_at` = NOW() where `name` like 'Kevin Geniets'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Sean Quinn"');
        DB::delete('delete from champions where name like "Alberto Bettiol"');
        DB::delete('delete from champions where name like "Arnaud de Lie"');
        DB::delete('delete from champions where name like "Dylan Groenewegen"');
        DB::delete('delete from champions where name like "Paul Lapeira"');
        DB::delete('delete from champions where name like "Alex Aranburu"');
        DB::update("update champions set `active` = 0, `team` = 'efe', `ranking` = 38, `full` = NULL, `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Rui Costa'");
        DB::update("update champions set `active` = 0, `full` = 'be', `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Remco Evenepoel'");
        DB::update("update champions set `active` = 0, `full` = 'fr', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Valentin Madouas'");
        DB::update("update champions set `active` = 0, `ranking` = 368, `full` = NULL, `left` = 'lu', `right` = 'lu', `collar` = NULL, `updated_at` = NOW() where `name` like 'Kevin Geniets'");
    }
};
