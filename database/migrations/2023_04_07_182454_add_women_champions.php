<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{

    private $champions = [
        ['name' => 'Riejanne Markus', 'championship' => 'women', 'team' => 'jvw', 'ranking' => 40, 'full' => 'nl', 'left' => 'nl', 'right' => 'nl', 'collar' => 'nl'],
        ['name' => 'Anouska Koster', 'championship' => 'women', 'team' => 'uxt', 'ranking' => 70, 'left' => 'nl', 'right' => 'nl'],
        ['name' => 'Elena Cecchini', 'championship' => 'women', 'team' => 'sdw', 'ranking' => 68, 'left' => 'it', 'right' => 'it', 'collar' => 'it'],
        ['name' => 'Alice Towers', 'championship' => 'women', 'team' => 'csr', 'ranking' => 224, 'full' => 'gb', 'left' => 'gb', 'right' => 'gb', 'collar' => 'gb'],
        ['name' => 'Hannah Barnes', 'championship' => 'women', 'team' => 'uxt', 'ranking' => 74, 'left' => 'gb2', 'right' => 'gb2'],
        ['name' => 'Brodie Chapman', 'championship' => 'women', 'team' => 'tfs', 'ranking' => 67, 'full' => 'au', 'left' => 'au-l', 'right' => 'au-l'],
        ['name' => 'Nicole Frain', 'championship' => 'women', 'team' => 'phv', 'ranking' => 178, 'left' => 'au-l', 'right' => 'au-l', 'collar' => 'au'],
        ['name' => 'Sarah Roy', 'championship' => 'women', 'team' => 'csr', 'ranking' => 132, 'right' => 'au'],
        ['name' => 'Emma Langley', 'championship' => 'women', 'team' => 'tib', 'ranking' => 134, 'full' => 'us'],
        ['name' => 'Audrey Cordon-Ragot', 'championship' => 'women', 'team' => 'hpw', 'ranking' => 25, 'full' => 'fr', 'left' => 'fr', 'right' => 'fr', 'collar' => 'fr'],
        ['name' => 'Évita Muzic', 'championship' => 'women', 'team' => 'fst', 'ranking' => 31, 'left' => 'fr', 'right' => 'fr'],
        ['name' => 'Kim de Baat', 'championship' => 'women', 'team' => 'fed', 'ranking' => 276, 'full' => 'be'],
        ['name' => 'Lotte Kopecky', 'championship' => 'women', 'team' => 'sdw', 'ranking' => 4, 'left' => 'be', 'right' => 'be', 'collar' => 'be'],
        ['name' => 'Liane Lippert', 'championship' => 'women', 'team' => 'mov', 'ranking' => 7, 'full' => 'de', 'left' => 'de', 'right' => 'de', 'collar' => 'de'],
        ['name' => 'Wiktoria Pikulik', 'championship' => 'women', 'team' => 'maw', 'ranking' => 92, 'full' => 'pl'],
        ['name' => 'Marta Lach', 'championship' => 'women', 'team' => 'wnt', 'ranking' => 57, 'left' => 'pl', 'right' => 'pl'],
        ['name' => 'Marlen Reusser', 'championship' => 'women', 'team' => 'sdw', 'ranking' => 28, 'left' => 'ch', 'right' => 'ch', 'collar' => 'ch'],
        ['name' => 'Mavi García', 'championship' => 'women', 'team' => 'liv', 'ranking' => 16, 'full' => 'es', 'left' => 'es', 'right' => 'es', 'collar' => 'es'],
        ['name' => 'Cecilie Uttrup Ludwig', 'championship' => 'women', 'team' => 'fst', 'ranking' => 9, 'full' => 'dk'],
        ['name' => 'Maggie Coles-Lyster', 'championship' => 'women', 'team' => 'zaf', 'ranking' => 264, 'full' => 'ca'],
        ['name' => 'Malin Eriksen', 'championship' => 'women', 'team' => 'dch', 'ranking' => 233, 'full' => 'no', 'left' => 'no', 'right' => 'no'],
        ['name' => 'Mie Bjørndal Ottestad', 'championship' => 'women', 'team' => 'uxt', 'ranking' => 630, 'left' => 'no', 'right' => 'no'],
        ['name' => 'Christina Schweinberger', 'championship' => 'women', 'team' => 'fed', 'ranking' => 61, 'full' => 'at', 'left' => 'at', 'right' => 'at', 'collar' => 'at'],
        ['name' => 'Christine Majerus', 'championship' => 'women', 'team' => 'sdw', 'ranking' => 76, 'full' => 'lu'],
        ['name' => 'Blanka Vas', 'championship' => 'women', 'team' => 'sdw', 'ranking' => 77, 'full' => 'hu'],
        ['name' => 'Tereza Neumanova', 'championship' => 'women', 'team' => 'liv', 'ranking' => 96, 'full' => 'cz', 'left' => 'cz', 'right' => 'cz'],
        ['name' => 'Jelena Erić', 'championship' => 'women', 'team' => 'mov', 'ranking' => 108, 'full' => 'rs', 'left' => 'rs', 'right' => 'rs', 'collar' => 'rs'],
        ['name' => 'Nesrine Houili', 'championship' => 'women', 'team' => 'csg', 'ranking' => 109, 'full' => 'dz'],
        ['name' => 'Catalina Anais Soto', 'championship' => 'women', 'team' => 'bdu', 'ranking' => 118, 'full' => 'cl'],
        ['name' => 'Eugenia Bujak', 'championship' => 'women', 'team' => 'uad', 'ranking' => 137, 'full' => 'si', 'left' => 'si', 'right' => 'si'],
        ['name' => 'Diana Peñuela', 'championship' => 'women', 'team' => 'dna', 'ranking' => 149, 'full' => 'co', 'left' => 'co-l', 'right' => 'co-l', 'collar' => 'co'],
        ['name' => 'Kathrin Schweinberger', 'championship' => 'women', 'team' => 'wnt', 'ranking' => 161, 'left' => 'at', 'right' => 'at'],
        ['name' => 'Anniina Ahtosalo', 'championship' => 'women', 'team' => 'uxt', 'ranking' => 166, 'full' => 'fi'],
        ['name' => 'Omer Shapira', 'championship' => 'women', 'team' => 'tib', 'ranking' => 174, 'full' => 'il', 'left' => 'il', 'right' => 'il'],
        ['name' => 'Daniela Campos', 'championship' => 'women', 'team' => 'bdu', 'ranking' => 198, 'full' => 'pt'],
        ['name' => 'Alison Jackson', 'championship' => 'women', 'team' => 'tib', 'ranking' => 64, 'left' => 'ca'],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->champions as $champion) {
            DB::insert("insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("
                . "'" . $champion['name'] . "', "
                . "'" . $champion['championship'] . "', "
                . "'" . $champion['team'] . "', "
                . "'" . $champion['ranking'] . "', "
                . "'" . ($champion['full'] ?? NULL) . "', "
                . "'" . ($champion['left'] ?? NULL) . "', "
                . "'" . ($champion['right'] ?? NULL) . "', "
                . "'" . ($champion['collar'] ?? NULL)
                . "')");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->champions as $champion) {
            DB::delete('delete from champions where `name` like ?', [$champion['name']]);
        }
    }
};
