<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private $champions = [
        ['name' => 'Yukiya Arashiro', 'championship' => 'men', 'team' => 'tbv', 'ranking' => 418, 'full' => 'jp', 'left' => 'jp', 'right' => 'jp'],
        ['name' => 'Mads Würtz Schmidt', 'championship' => 'men', 'team' => 'ipt', 'ranking' => 1063, 'left' => 'dk', 'right' => 'dk'],
        ['name' => 'Itamar Einhorn', 'championship' => 'men', 'team' => 'ipt', 'ranking' => 257, 'full' => 'il', 'left' => 'il2', 'right' => 'il2'],
        ['name' => 'Ahmed Madan', 'championship' => 'men', 'team' => 'tbv', 'ranking' => 543, 'full' => 'bh', 'left' => 'bh', 'right' => 'bh'],
    ];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach ($this->champions as $champion) {
            DB::insert("insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("
                . "'" . $champion['name'] . "', "
                . "'" . $champion['championship'] . "', "
                . "'" . $champion['team'] . "', "
                . "'" . $champion['ranking'] . "', "
                . "'" . ($champion['full'] ?? NULL) . "', "
                . "'" . ($champion['left'] ?? NULL) . "', "
                . "'" . ($champion['right'] ?? NULL) . "', "
                . "'" . ($champion['collar'] ?? NULL)
                . "')");
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        foreach ($this->champions as $champion) {
            DB::delete('delete from champions where `name` like ?', [$champion['name']]);
        }
    }
};
