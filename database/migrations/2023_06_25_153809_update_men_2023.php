<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    private $stillChampions = [
        "Remco Evenepoel",
        "Fabio Jakobsen",
        "Attila Valter",
        "Itamar Einhorn",
        "Richard Carapaz",
        "Luke Plapp",
        "Esteban Chaves",
        "Orluis Aular",
    ];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Deactivate every 2022 champions
        DB::update('update champions set active = 0 where championship = \'men\' and full <> \'\'');

        // Reactivate every champion still champion in 2023
        foreach ($this->stillChampions as $champion) {
            DB::update('update champions set active = 1 where name = \'' . $champion . '\'');
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {

        // Deactivate every champion still champion in 2023
        foreach ($this->stillChampions as $champion) {
            DB::update('update champions set active = 0 where name = \'' . $champion . '\'');
        }

        // Reactivate every 2022 champions
        DB::update('update champions set active = 1 where championship = \'men\' and full <> \'\'');
    }
};
