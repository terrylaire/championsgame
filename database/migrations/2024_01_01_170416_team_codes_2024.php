<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `team` = 'dat' where `team` like 'act'");
        DB::update("update champions set `team` = 'iwa' where `team` like 'icw'");
        DB::update("update champions set `team` = 'dfp' where `team` like 'dsm'");
        DB::update("update champions set `team` = 'tvl' where `team` like 'tjv'");
        DB::update("update champions set `team` = 'tvl' where `team` like 'jvw'");
        DB::update("update champions set `team` = 'uxm' where `team` like 'uxt'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `team` = 'act' where `team` like 'dat'");
        DB::update("update champions set `team` = 'icw' where `team` like 'iwa'");
        DB::update("update champions set `team` = 'dsm' where `team` like 'dfp'");
        DB::update("update champions set `team` = 'tjv' where `team` like 'tvl' and `championship` like 'men'");
        DB::update("update champions set `team` = 'jvw' where `team` like 'tvl' and `championship` like 'women'");
        DB::update("update champions set `team` = 'uxt' where `team` like 'uxm'");
    }
};
