<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Olivia Baril", "women", "mov", 35, "ca", "ca", "ca", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Kristen Faulkner", "women", "eoc", 161, "us", "olym", "olym", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Mauro Schmid", "men", "jay", 96, "ch", "ch", "ch", NULL)');
        DB::update("update champions set `active` = 1, `team` = 'fed', `ranking` = 157, `full` = NULL, `left` = 'at', `right` = 'at', `collar` = NULL, `updated_at` = NOW() where `name` like 'Carina Schrempf'");
        DB::update("update champions set `active` = 1, `team` = 'sdw', `ranking` = 1, `full` = NULL, `left` = 'nl', `right` = 'nl', `collar` = 'nl', `updated_at` = NOW() where `name` like 'Demi Vollering'");
        DB::update("update champions set `active` = 1, `team` = 'sdw', `ranking` = 82, `full` = NULL, `left` = 'lu', `right` = 'lu', `collar` = 'lu', `updated_at` = NOW() where `name` like 'Christine Majerus'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Olivia Baril"');
        DB::delete('delete from champions where name like "Kristen Faulkner"');
        DB::delete('delete from champions where name like "Mauro Schmid"');
        DB::update("update champions set `active` = 0, `team` = 'fed', `ranking` = 157, `full` = 'at', `left` = 'at', `right` = 'at', `collar` = NULL, `updated_at` = NOW() where `name` like 'Carina Schrempf'");
        DB::update("update champions set `active` = 0, `team` = 'sdw', `ranking` = 1, `full` = 'nl', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Demi Vollering'");
        DB::update("update champions set `active` = 0, `team` = 'sdw', `ranking` = 82, `full` = 'lu', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Christine Majerus'");
    }
};
