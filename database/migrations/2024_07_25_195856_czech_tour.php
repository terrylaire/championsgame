<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Stanislaw Aniolkowski", "men", "cof", 301, NULL, "pl", "pl", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Jacob Eriksson", "men", "tud", 2439, "se", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Markus Hoelgaard", "men", "uxm", 2351, "no", NULL, NULL, NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Marco Brenner", "men", "tud", 862, "de", "de", "de", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Alexander Hajek", "men", "boh", 974, "at", "at", "at", "at")');
        DB::update("update champions set `active` = 1, `team` = 'uad', `ranking` = 1853, `full` = 'si', `left` = 'si', `right` = 'si', `collar` = NULL, `updated_at` = NOW() where `name` like 'Domen Novak'");
        DB::update("update champions set `active` = 1, `team` = 'q36', `ranking` = 3001, `full` = 'et', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Negasi Haylu Abreha'");
        DB::update("update champions set `active` = 1, `team` = 'soq', `ranking` = 88, `full` = NULL, `left` = 'world', `right` = 'world', `collar` = NULL, `updated_at` = NOW() where `name` like 'Julian Alaphilippe'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Stanislaw Aniolkowski"');
        DB::delete('delete from champions where name like "Jacob Eriksson"');
        DB::delete('delete from champions where name like "Markus Hoelgaard"');
        DB::delete('delete from champions where name like "Marco Brenner"');
        DB::delete('delete from champions where name like "Alexander Hajek"');
        DB::update("update champions set `active` = 0, `team` = 'uad', `ranking` = 1853, `full` = NULL, `left` = 'si', `right` = 'si', `collar` = NULL, `updated_at` = NOW() where `name` like 'Domen Novak'");
        DB::update("update champions set `active` = 1, `team` = 'q36', `ranking` = 1439, `full` = NULL, `left` = 'et', `right` = 'et', `collar` = NULL, `updated_at` = NOW() where `name` like 'Negasi Haylu Abreha'");
        DB::update("update champions set `active` = 1, `team` = 'soq', `ranking` = 88, `full` = NULL, `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Julian Alaphilippe'");
    }
};
