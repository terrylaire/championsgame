<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Oier Lazkano", "men", "mov", 670, "es", "es", "es", "es", NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Fredrik Dversnes", "men", "uxt", 1225, "no", NULL, NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Simone Velasco", "men", "ast", 373, "it", NULL, NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Lucas Eriksson", "men", "tud", 341, "se", NULL, NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Emīls Liepiņš", "men", "ltk", 334, "lv", NULL, NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Dušan Rajović", "men", "tbv", 405, "rs", NULL, NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Gleb Brussenskiy", "men", "ast", 370, "asia", NULL, NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Marc Hirschi", "men", "uad", 60, "ch", "ch-l", "ch-l", NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Rein Taaramäe", "men", "icw", 371, NULL, "ee", "ee", NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Jonathan Klever Caicedo", "men", "efe", 580, NULL, "ec", NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Nickolas Zukowsky", "men", "q36", 288, "ca", NULL, NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Rebecca Koerner", "women", "uxt", 616, "dk", NULL, NULL, NULL, NOW(), NOW())');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`, `created_at`, `updated_at`) values ("Emilia Fahlin", "women", "fst", 94, "se", NULL, NULL, NULL, NOW(), NOW())');
        DB::update("update champions set `active` = 1, `full` = NULL, `updated_at` = NOW() where `name` like 'João Almeida'");
        DB::update("update champions set `active` = 1, `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Remco Evenepoel'");
        DB::update("update champions set `active` = 1, `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Annemiek van Vleuten'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Oier Lazkano"');
        DB::delete('delete from champions where name like "Fredrik Dversnes"');
        DB::delete('delete from champions where name like "Simone Velasco"');
        DB::delete('delete from champions where name like "Lucas Eriksson"');
        DB::delete('delete from champions where name like "Emīls Liepiņš"');
        DB::delete('delete from champions where name like "Dušan Rajović"');
        DB::delete('delete from champions where name like "Gleb Brussenskiy"');
        DB::delete('delete from champions where name like "Marc Hirschi"');
        DB::delete('delete from champions where name like "Rein Taaramäe"');
        DB::delete('delete from champions where name like "Jonathan Klever Caicedo"');
        DB::delete('delete from champions where name like "Nickolas Zukowsky"');
        DB::delete('delete from champions where name like "Rebecca Koerner"');
        DB::delete('delete from champions where name like "Emilia Fahlin"');
        DB::update("update champions set `active` = 0, `full` = 'pt', `updated_at` = NOW() where `name` like 'João Almeida'");
        DB::update("update champions set `active` = 1, `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Remco Evenepoel'");
        DB::update("update champions set `active` = 0, `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Annemiek van Vleuten'");
    }
};
