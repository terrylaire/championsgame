<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Krists Neilands", "men", "ipt", 228, NULL, "lv", "lv", NULL)');
        DB::update("update champions set `active` = 1, `team` = 'ipt', `ranking` = 137, `full` = NULL, `left` = 'de', `right` = 'de', `collar` = NULL, `updated_at` = NOW() where `name` like 'Pascal Ackermann'");
        DB::update("update champions set `active` = 1, `team` = 'ast', `ranking` = 1107, `full` = NULL, `left` = 'dk', `right` = 'dk', `collar` = NULL, `updated_at` = NOW() where `name` like 'Michael Mørkøv'");
        DB::update("update champions set `active` = 1, `team` = 'ipt', `ranking` = 164, `full` = NULL, `left` = 'it', `right` = 'it', `collar` = NULL, `updated_at` = NOW() where `name` like 'Giacomo Nizzolo'");
        DB::update("update champions set `active` = 0 where name like 'Diana Peñuela'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Krists Neilands"');
        DB::update("update champions set `active` = 0, `team` = 'uad', `ranking` = 171, `full` = NULL, `left` = 'de', `right` = 'de', `collar` = NULL, `updated_at` = NOW() where `name` like 'Pascal Ackermann'");
        DB::update("update champions set `active` = 1, `team` = 'soq', `ranking` = 1107, `full` = NULL, `left` = 'dk', `right` = 'dk', `collar` = NULL, `updated_at` = NOW() where `name` like 'Michael Mørkøv'");
        DB::update("update champions set `active` = 1, `team` = 'ipt', `ranking` = 164, `full` = NULL, `left` = NULL, `right` = 'it', `collar` = NULL, `updated_at` = NOW() where `name` like 'Giacomo Nizzolo'");
        DB::update("update champions set `active` = 1 where name like 'Diana Peñuela'");
    }
};
