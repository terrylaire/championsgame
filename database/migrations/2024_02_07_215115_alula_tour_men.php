<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `team` = 'dfp', `ranking` = 359, `full` = 'lv', `left` = 'lv', `right` = 'lv', `collar` = NULL, `updated_at` = NOW() where `name` like 'Emīls Liepiņš'");
        DB::update("update champions set `active` = 1, `team` = 'ipt', `ranking` = 289, `full` = NULL, `left` = 'nz', `right` = 'nz', `collar` = NULL, `updated_at` = NOW() where `name` like 'George Bennett'");
        DB::update("update champions set `active` = 1, `team` = 'uxm', `ranking` = 70, `full` = NULL, `left` = 'no', `right` = 'no', `collar` = NULL, `updated_at` = NOW() where `name` like 'Rasmus Tiller'");
        DB::update("update champions set `active` = 1, `team` = 'tud', `ranking` = 92, `full` = NULL, `left` = 'euro', `right` = 'euro', `collar` = NULL, `updated_at` = NOW() where `name` like 'Matteo Trentin'");
        DB::update("update champions set `active` = 1, `team` = 'bah', `ranking` = 2532, `full` = 'bh', `left` = 'bh', `right` = 'bh', `collar` = NULL, `updated_at` = NOW() where `name` like 'Ahmed Madan'");
        DB::update("update champions set `active` = 1, `team` = 'dat', `ranking` = 429, `full` = NULL, `left` = 'be', `right` = 'be', `collar` = NULL, `updated_at` = NOW() where `name` like 'Dries De Bondt'");
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Ivo Oliveira", "men", "uad", 331, "pt", "pt", "pt", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Henok Mulubrhan", "men", "ast", 107, "africa", "ac-l", "ac-l", "africa")');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 0, `team` = 'ltk', `ranking` = 359, `full` = 'lv', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Emīls Liepiņš'");
        DB::update("update champions set `active` = 0, `team` = 'uad', `ranking` = 204, `full` = NULL, `left` = 'nz', `right` = 'nz', `collar` = NULL, `updated_at` = NOW() where `name` like 'George Bennett'");
        DB::update("update champions set `active` = 0, `team` = 'uxm', `ranking` = 116, `full` = 'no', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Rasmus Tiller'");
        DB::update("update champions set `active` = 0, `team` = 'tud', `ranking` = 92, `full` = NULL, `left` = 'euro', `right` = 'euro', `collar` = NULL, `updated_at` = NOW() where `name` like 'Matteo Trentin'");
        DB::update("update champions set `active` = 0, `team` = 'bah', `ranking` = 543, `full` = 'bh', `left` = 'bh', `right` = 'bh', `collar` = NULL, `updated_at` = NOW() where `name` like 'Ahmed Madan'");
        DB::update("update champions set `active` = 0, `team` = 'adc', `ranking` = 101, `full` = NULL, `left` = 'be', `right` = 'be', `collar` = NULL, `updated_at` = NOW() where `name` like 'Dries De Bondt'");
        DB::delete('delete from champions where name like "Ivo Oliveira"');
        DB::delete('delete from champions where name like "Henok Mulubrhan"');
    }
};
