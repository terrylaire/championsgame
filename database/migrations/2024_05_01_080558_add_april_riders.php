<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Sam Bennett", "men", "dat", 314, NULL, "ie", "ie", NULL)');
        DB::update("update champions set `active` = 1, `team` = 'ltk', `ranking` = 1379, `full` = 'za', `left` = 'za', `right` = 'za', `collar` = NULL, `updated_at` = NOW() where `name` like 'Ryan Gibbons'");
        DB::update("update champions set `active` = 1, `team` = 'igd', `ranking` = 336, `full` = NULL, `left` = 'no', `right` = 'no', `collar` = NULL, `updated_at` = NOW() where `name` like 'Tobias Foss'");
        DB::update("update champions set `active` = 1, `team` = 'efe', `ranking` = 81, `full` = NULL, `left` = 'ec', `right` = 'ec', `collar` = NULL, `updated_at` = NOW() where `name` like 'Richard Carapaz'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Sam Bennett"');
        DB::update("update champions set `active` = 0, `team` = 'uad', `ranking` = 424, `full` = NULL, `left` = 'ac-l', `right` = 'ac-l', `collar` = NULL, `updated_at` = NOW() where `name` like 'Ryan Gibbons'");
        DB::update("update champions set `active` = 0, `team` = 'tvl', `ranking` = 112, `full` = NULL, `left` = 'no', `right` = 'no', `collar` = 'no', `updated_at` = NOW() where `name` like 'Tobias Foss'");
        DB::update("update champions set `active` = 0, `team` = 'efe', `ranking` = 81, `full` = 'ec', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Richard Carapaz'");
    }
};
