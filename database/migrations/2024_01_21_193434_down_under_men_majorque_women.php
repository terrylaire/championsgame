<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Jhonatan Narváez", "men", "igd", 147, NULL, "ec", "ec", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Guillaume Boivin", "men", "ipt", 670, NULL, "ca", "ca", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Arlenis Sierra", "women", "mov", 36, "cu", "cu", "cu", "cu")');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Ashleigh Moolman", "women", "ags", 19, NULL, "za", "za", NULL)');
        DB::insert('insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("Luyao Zeng", "women", "win", 134, "cn", NULL, NULL, NULL)');
        DB::update("update champions set `active` = 1, `team` = 'cgs', `ranking` = 131, `full` = 'cy', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Antri Christoforou'");
        DB::update("update champions set `active` = 1, `team` = 'ark', `ranking` = 127, `full` = 'se', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Emilia Fahlin'");
        DB::update("update champions set `active` = 1, `ranking` = 203, `updated_at` = NOW() where name like 'Luke Plapp'");
        DB::update("update champions set `active` = 1, `ranking` = 152, `updated_at` = NOW() where name like 'Jelena Erić'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::delete('delete from champions where name like "Jhonatan Narváez"');
        DB::delete('delete from champions where name like "Guillaume Boivin"');
        DB::delete('delete from champions where name like "Arlenis Sierra"');
        DB::delete('delete from champions where name like "Ashleigh Moolman"');
        DB::delete('delete from champions where name like "Luyao Zeng"');
        DB::update("update champions set `active` = 0, `team` = 'hph', `ranking` = 131, `full` = 'cy', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Antri Christoforou'");
        DB::update("update champions set `active` = 0, `team` = 'fst', `ranking` = 94, `full` = 'se', `left` = NULL, `right` = NULL, `collar` = NULL, `updated_at` = NOW() where `name` like 'Emilia Fahlin'");
        DB::update("update champions set `active` = 0, `ranking` = 151, `updated_at` = NOW() where name like 'Luke Plapp'");
        DB::update("update champions set `active` = 0, `ranking` = 108, `updated_at` = NOW() where name like 'Jelena Erić'");
    }
};
