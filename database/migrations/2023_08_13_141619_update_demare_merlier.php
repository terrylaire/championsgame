<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `team` = 'ark', `updated_at` = NOW() where `name` like 'Arnaud Démare'");
        DB::update("update champions set `full` = NULL, `left` = 'be', `right` = 'be', `collar` = NULL, `active` = 1, `updated_at` = NOW() where `name` like 'Tim Merlier'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `team` = 'gfc', `updated_at` = NOW() where `name` like 'Arnaud Démare'");
        DB::update("update champions set `full` = 'be', `left` = NULL, `right` = NULL, `collar` = NULL, `active` = 0, `updated_at` = NOW() where `name` like 'Tim Merlier'");
    }
};
