<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private $champions = [
        ['name' => 'Tadej Pogačar', 'championship' => 'men', 'team' => 'uad', 'ranking' => 1, 'full' => 'si', 'left' => 'si', 'right' => 'si'],
        ['name' => 'Dylan van Baarle', 'championship' => 'men', 'team' => 'tjv', 'ranking' => 43, 'full' => 'nl', 'collar' => 'nl'],
        ['name' => 'Fred Wright', 'championship' => 'men', 'team' => 'tbv', 'ranking' => 185, 'full' => 'gb', 'left' => 'gb', 'right' => 'gb'],
        ['name' => 'Emanuel Buchmann', 'championship' => 'men', 'team' => 'boh', 'ranking' => 148, 'full' => 'de', 'left' => 'de', 'collar' => 'de'],
        ['name' => 'Alexey Lutsenko', 'championship' => 'men', 'team' => 'ast', 'ranking' => 147, 'full' => 'kz'],
        ['name' => 'Gregor Mühlberger', 'championship' => 'men', 'team' => 'mov', 'ranking' => 849, 'full' => 'at', 'left' => 'at', 'right' => 'at', 'collar' => 'at'],
        ['name' => 'Mattias Skjelmose', 'championship' => 'men', 'team' => 'ltk', 'ranking' => 51, 'full' => 'dk'],
        ['name' => 'Alex Kirsch', 'championship' => 'men', 'team' => 'ltk', 'ranking' => 830, 'full' => 'lu'],
        ['name' => 'Quinn Simmons', 'championship' => 'men', 'team' => 'ltk', 'ranking' => 482, 'full' => 'us'],
    ];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 1, `full` = null, `left` = 'fr', `right` = 'fr', `updated_at` = NOW() where `name` like 'Florian Sénéchal'");
        DB::update("update champions set `active` = 1, `full` = null, `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Mark Cavendish'");

        foreach ($this->champions as $champion) {
            DB::insert("insert into champions(`name`, `championship`, `team`, `ranking`, `full`, `left`, `right`, `collar`) values ("
                . "'" . $champion['name'] . "', "
                . "'" . $champion['championship'] . "', "
                . "'" . $champion['team'] . "', "
                . "'" . $champion['ranking'] . "', "
                . "'" . ($champion['full'] ?? NULL) . "', "
                . "'" . ($champion['left'] ?? NULL) . "', "
                . "'" . ($champion['right'] ?? NULL) . "', "
                . "'" . ($champion['collar'] ?? NULL)
                . "')");
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 0, `full` = 'fr', `left` = '', `right` = '', `updated_at` = NOW() where `name` like 'Florian Sénéchal'");
        DB::update("update champions set `active` = 0, `full` = 'gb', `left` = 'world', `right` = 'world', `collar` = 'world', `updated_at` = NOW() where `name` like 'Mark Cavendish'");

        foreach ($this->champions as $champion) {
            DB::delete('delete from champions where `name` like ?', [$champion['name']]);
        }
    }
};
