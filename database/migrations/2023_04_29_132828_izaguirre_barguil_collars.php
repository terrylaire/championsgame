<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set collar = 'es' where name like 'Ion Izagirre'");
        DB::update("update champions set collar = '' where name like 'Warren Barguil'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set collar = '' where name like 'Ion Izagirre'");
        DB::update("update champions set collar = 'fr' where name like 'Warren Barguil'");
    }
};
