<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::update("update champions set `active` = 0 where name like 'Adam de Vos'");
        DB::update("update champions set `active` = 0 where name like 'Hannah Barnes'");
        DB::update("update champions set `active` = 0 where name like 'Luis León Sánchez'");
        DB::update("update champions set `active` = 0 where name like 'Michael Schär'");
        DB::update("update champions set `active` = 0 where name like 'Kim de Baat'");
        DB::update("update champions set `active` = 0 where name like 'Annemiek van Vleuten'");
        DB::update("update champions set `active` = 0 where name like 'Peter Sagan'");
        DB::update("update champions set `active` = 0 where name like 'Kristijan Koren'");
        DB::update("update champions set `active` = 0 where name like 'Nacer Bouhanni'");
        DB::update("update champions set `active` = 0 where name like 'Jan Bárta'");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::update("update champions set `active` = 1 where name like 'Adam de Vos'");
        DB::update("update champions set `active` = 1 where name like 'Hannah Barnes'");
        DB::update("update champions set `active` = 1 where name like 'Luis León Sánchez'");
        DB::update("update champions set `active` = 1 where name like 'Michael Schär'");
        DB::update("update champions set `active` = 1 where name like 'Kim de Baat'");
        DB::update("update champions set `active` = 1 where name like 'Annemiek van Vleuten'");
        DB::update("update champions set `active` = 1 where name like 'Peter Sagan'");
        DB::update("update champions set `active` = 1 where name like 'Kristijan Koren'");
        DB::update("update champions set `active` = 1 where name like 'Nacer Bouhanni'");
        DB::update("update champions set `active` = 1 where name like 'Jan Bárta'");
    }
};
