<?php

return [

    'all-champions-title' => 'Every champion',
    'all-champions-subtitle' => 'of cycling available in a game of Wholeur',
    'all-champions-play' => 'Play now!',

    'announce' => 'Wholeur was a game using the cycling champions of 2023 and 2024. It is now archived and reezed with the informations of 2024.',

    'championship-women-initial' => 'W',
    'championship-women-jersey' => 'Women',
    'championship-men-initial' => 'M',
    'championship-men-jersey' => 'Men',

    'faq-question1' => 'How to play?',
    'faq-answer1' => 'The game show you a professionnal cyclist\'s champion jersey. You have to identify its owner with the hints on it: the team, the colors, the streaks, the collar… The games also tells you if the owner is a woman or a man. Start typing a name in the dedicated input under the jersey, and the game should suggest you names. Choose a name among the suggestions, then click on the button with a cyclist on it to confirm. If you guessed right, you score a point. Otherwise, no point. The answer is given as well as the next jersey. Guess as many jerseys as the difficulty level calls for, and get the best score! Try again to get better.',
    'faq-question6' => 'With which jerseys is it played?',
    'faq-answer6' => 'With the jerseys of 2024? since this game is archived. You can browse the <a href="/en/every-national-champion" title="Wholeur - Every national champion jersey">list of the champions\' jerseys</a> available in the game.',
    'faq-question2' => 'Are there duplicates?',
    'faq-answer2' => 'Several jerseys of forme champions in the same team are indeed the same. They can appear multiple times. Each time they appear, you can guess any of the acceptable answer.',
    'faq-question3' => 'Is there the Women\'s tour leader jersey?',
    'faq-answer3' => 'The leader of the women\'s tour wears a special jersey as long as she is ahead on this always-updated ranking. This jersey is not part of the game, and the usual jersey of its owner stays in the game.',
    'faq-question4' => 'How is this made ?',
    'faq-answer4' => 'You can learn about it on',
    'faq-question5' => 'Can we have a chat?',
    'faq-answer5' => 'We can! My contact is on my personal web page, linked at the bottom of this one. You can also use the issues on ',

    'footer-developer-before' => 'Yet another game by ',
    'footer-developer-after' => '',

    'home' => 'Home',
    'meta-description' => 'Play the game and guess the pro cyclists by inspecting their jerseys. Find the right team, the right country and score the maximum!',
    'meta-keywords' => 'Cycling, game, jersey, champion, guessing, UCI, World Tour, Tour de France, Giro, Vuelta',
    'meta-title' => 'Wholeur: guess the pro cyclist from their jersey. A game of memory for professional cycling enthousiasts',
    'nojavascript' => 'Warning: Javascript seems absent or deactivated here. You will not be able to play the game without it.',

    'options-championship-both' => 'Both',
    'options-championship-women' => 'Women',
    'options-championship-men' => 'Men',

    'over-message' => 'Congrats! You identified :score cyclists out of :goal!',
    'retry' => 'Retry',
    'share' => 'Share',
    'sharing-text' => 'I identified :score professional cyclists on https://wholeur.terrylaire.fr/en',
    'tagline' => 'Guess the cyclists from their champion\'s jersey',

    'validation-unknown-champion' => 'Please select a champion in the list',

    'who-wears-it' => 'Who wore it in 2024?',
];
