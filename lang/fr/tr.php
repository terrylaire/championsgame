<?php

return [

    'all-champions-title' => 'Toustes les champion-ne-s',
    'all-champions-subtitle' => 'cyclistes professionnel-le-s présent-e-s dans une partie de Wholeur',
    'all-champions-play' => 'Jouez maintenant !',

    'announce' => 'Wholeur était un jeu avec les cyclistes professionnel-les en 2023 et 2024. Le jeu est maintenant une archive arrêtée avec les informations de 2024.',

    'championship-women-initial' => 'F',
    'championship-women-jersey' => 'Femmes',
    'championship-men-initial' => 'H',
    'championship-men-jersey' => 'Hommes',

    'faq-question1' => 'Comment jouer ?',
    'faq-answer1' => 'Le jeu vous présente un maillot de cycliste professionnel en activité. Vous devez identifier son ou sa propriétaire grâce aux indices présents sur le maillot : son équipe, son motif, ses liserés et/ou son col. Le jeu vous indique aussi s\'il s\'agit du maillot d\'une femme ou d\'un homme. Commencez à écrire un nom dans le champ dédié sous le maillot, et le jeu vous proposera des réponses correspondantes. Choisissez une réponse parmi celles proposées, et cliquez sur le bouton montrant un-e cycliste pour valider. Si vous avez juste, vous marquez un point. Si vous avez faux, vous ne marquez pas de point. La réponse vous est donnée en même temps que le maillot suivant. Répondez à autant de maillots que demandés par votre niveau de difficulté, et faites le meilleur score possible ! Recommencez pour vous améliorer.',
    'faq-question6' => 'Avec quels maillots on joue ?',
    'faq-answer6' => 'Avec les maillots de 2024, puisque ce jeu est une archive. Vous pouvez consulter la page avec <a href="/every-national-champion" title="Wholeur - Tous les maillots de champions et championnes de cyclisme">la liste des maillots</a> pour tout savoir.',
    'faq-question2' => 'Il y a des maillots en double ?',
    'faq-answer2' => 'Quelques maillots d\'ancien-ne-s champion-ne-s identiques sont portés par plusieurs cyclistes de la même équipe. Ces maillots peuvent apparaitre plusieurs fois dans le jeu. Vous pouvez donner n\'importe laquelle des réponses acceptables pour ces maillots à chaque fois qu\'ils apparaissent.',
    'faq-question3' => 'Et le maillot de leader du Women\'s tour ?',
    'faq-answer3' => 'Dans le championnat féminin de l\'UCI, la cycliste en tête porte un maillot spécial tant qu\'elle reste première du championnat remis à jour régulièrement. Ce maillot ne fait pas partie du jeu, et le maillot que la coureuse porte habituellement reste présent dans le jeu.',
    'faq-question4' => 'Comment c\'est codé ?',
    'faq-answer4' => 'Vous pouvez le voir sur ',
    'faq-question5' => 'On peut discuter, peut-être ?',
    'faq-answer5' => 'On peut ! Vous trouverez mon contact sur mon site web lié en bas de cette page. Vous pouvez aussi ouvrir une issue sur ',

    'footer-developer-before' => 'Encore un jeu de ',
    'footer-developer-after' => '',

    'home' => 'Accueil',
    'meta-description' => 'Jouez à deviner à qui sont ces maillots de cyclistes professionnel-le-s ! Retrouvez l\'équipe, le pays et marquez le plus de points !',
    'meta-keywords' => 'Cyclisme, jeu, maillots, champion, championne, deviner, devinette, UCI, World Tour, Tour de France, Tour d\'italie, Giro, Tour d\'Espagne, Vuelta',
    'meta-title' => 'Wholeur : devinez à qui est le maillot de cycliste professionnel-le. Un jeu pour les amateurs de cyclisme professionnel',
    'nojavascript' => 'Attention : Javascript semble absent ou désactivé. Vous ne pourrez pas jouer sans Javascript.',

    'options-championship-both' => 'Toustes',
    'options-championship-women' => 'Femmes',
    'options-championship-men' => 'Hommes',

    'over-message' => 'Bravo ! Vous avez identifié :score cyclistes sur :goal !',
    'retry' => 'Encore',
    'share' => 'Copier',
    'sharing-text' => 'J\'ai reconnu les maillots de :score cyclistes sur https://wholeur.terrylaire.fr !',
    'tagline' => 'Reconnaissez les maillots de champions des cyclistes',

    'validation-unknown-champion' => 'Choisissez un-e cycliste depuis la liste',

    'who-wears-it' => 'Qui le portait en 2024 ?',
];
