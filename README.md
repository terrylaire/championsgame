# Wholeur

Wholeur est un jeu sur le cyclisme jouable à l'adresse https://wholeur.terrylaire.fr

# Licence

Le code de Wholeur appartient à Terry Laire et n'est pas placé sous une licence libre. Vous pouvez me contacter si vous souhaitez l'utiliser et publier le résultat.

# Bugs, suggestions, merge requests

Si vous remarquez un problème et que vous avez lu la FAQ dans le jeu, vous pouvez ouvrir une issue sur [le Gitlab du jeu](https://gitlab.com/terrylaire/championsgame). Si vous avez une suggestion, vous pouvez également la faire au même endroit, bien que je ne cherche pas à faire évoluer ce jeu.

Si vous avez développé une modification au jeu, vous pouvez ouvrir une merge request. Vous serez bienvenue si vous avez amélioré une traduction ou un maillot ; pour des évolutions du jeu, postez une issue d'abord.

Quoi qu'il arrive, vous pouvez aussi m'écrire [sur Mastodon](https://framapiaf.org/terrylaire) ou à mon adresse email évoquée en bas de [mon site Internet](https://terrylaire.fr).

# Utilisation

Si vous souhaitez éxecuter le projet sur votre propre environnement, vous devez savoir qu'il s'agit d'un programme écrit en PHP avec le framework Laravel dont [la documentation](https://laravel.com/docs) pourrait vous être utile. Vous aurez aussi besoin d'une base de données relationnelle, de composer et de npm.

Une fois téléchargé, copiez le fichier en `.env.example`, renommez le `.env` et remplacez les valeurs de configuration par celle de votre environnement. Vous devriez ensuite pouvoir utiliser en ligne de commandes :

`composer install` pour télécharger les dépendances PHP

`npm install` pour télécharger les dépendances Node (essentiellement Vite)

`php artisan key:generate` pour écrire la key du fichier .env

`php artisan migrate` pour remplir la base de données

`php artisan duplicates:mark` pour marquer les coureurs similaires dans la base de données

Puis, pour lancer l'application :

`npm run dev` qui compile les assets JS et CSS

`php artisan serve` qui déploie l'application sur un serveur auquel vous pourrez accéder à l'adresse localhost:8000

# Speak english?

🇬🇧 If you understood any of this and want to contact me by any mean, you can do it in french or english.