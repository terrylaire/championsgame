<?php

namespace App\Console\Commands;

use App\Models\Champion;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MarkDuplicates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duplicates:mark';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark duplicates amongst riders  (riders with same jersey) in database';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Reset duplicate status for everyone
        DB::update('update champions set duplicate = 0');

        // For all active champions
        $allChamps = Champion::where('active', 1)->get();
        foreach ($allChamps as $champion) {
            // Ignore already marked champions
            if (!$champion->duplicate) {
                // Find identical champions
                Champion::where([
                    'championship' => $champion->championship,
                    'team' => $champion->team,
                    'full' => $champion->full,
                    'left' => $champion->left,
                    'right' => $champion->right,
                    'collar' => $champion->collar,
                    'active' => true,
                ])
                    ->whereNot(function ($query) use ($champion) {
                        $query->where('id', $champion->id);
                    })
                    ->update(['duplicate' => 1]);
            }
        }

        // Print duplicates
        echo "Duplicates are : \n";
        $dup = Champion::where('duplicate', 1)
                        ->orderBy('championship')
                        ->orderBy('team')
                        ->orderBy('full')
                        ->orderBy('left')
                        ->orderBy('right')
                        ->orderBy('collar')
                        ->get();
        foreach ($dup as $champion) {
            echo $champion->name . "\t\t" . $champion->team . "\t" . $champion->full . "\t" . $champion->left . "\t" . $champion->left . "\n";
        }
    }
}
