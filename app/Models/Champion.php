<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Champion extends Model
{
    /**
     * Get the URL of the jersey of a champion
     */
    public function getJerseyUrl(): string
    {
        // The path is /men or /women
        $path = '/img/champion-jerseys/' . $this->championship;

        // The path depends if the jersey is fully dedicated to the championship
        if ($this->full) {
            // The path is /full
            $path .= '/full';
            // Then the pattern
            $path .= '/' . $this->full;

        } else {
            // The path is the team
            $path .= '/' . strtolower($this->team);
        }
        $path .= '.png';

        return $path;
    }

    /**
     * Return URL of the left arm layer if it exists, null otherwise
     */
    public function getLeftJerseyUrl(): ?string
    {
        if ($this->left) {
            return '/img/champion-jerseys/left/' . $this->left . '.png';
        }

        return null;
    }

    /**
     * Return URL of the right arm layer if it exists, null otherwise
     */
    public function getRightJerseyUrl(): ?string
    {
        if ($this->right) {
            return '/img/champion-jerseys/right/' . $this->right . '.png';
        }

        return null;
    }

    /**
     * Return URL of the collar layer if it exists, null otherwise
     */
    public function getCollarJerseyUrl(): ?string
    {
        if ($this->collar) {
            return '/img/champion-jerseys/collar/' . $this->collar . '.png';
        }

        return null;
    }
}
