<?php

namespace App\Enums;

enum Championship: string
{
    case ALL = 'all';
    case WOMEN = 'women';
    case MEN = 'men';
}