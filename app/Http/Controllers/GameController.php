<?php

namespace App\Http\Controllers;

use App\Enums\Championship;
use App\Enums\Difficulty;
use App\Http\Requests\StartGameRequest;
use App\Models\Champion;
use App\Models\Game;
use DateTime;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\View\View;

/**
 * Controller for the game
 */
class GameController extends Controller
{

    /**
     * Number of champions to guess in each championship and difficulty
     */
    protected $championsCount = [
        'all' => ['easy' => 20, 'medium' => 50, 'hard' => 100],
        'women' => ['easy' => 10, 'medium' => 25, 'hard' => 50],
        'men' => ['easy' => 10, 'medium' => 25, 'hard' => 50],
    ];

    /**
     * Init the game with params
     */
    public function initGame(StartGameRequest $request): RedirectResponse
    {
        $this->createGameInDbAndSession($request);
        return redirect()->route('showGame');
    }

    /**
     * Show the game screen
     */
    public function showGame(Request $request): View
    {
        // If the game has not started yet
        if (!$request->session()->has('answers')
            || !$request->session()->has('suggestionsjs')
            ) {
            // Create it
            $this->createGameInDbAndSession($request);
        }

        // The game has been created
        $progress = session('progress', 0);
        $score = session('score', 0);
        $answers = session('answers');

        // Security if progress goes too high
        if ($progress >= count($answers)-1) {
            $progress = 0;
        }

        // Retrieve new champion to guess
        $futureChampionName = $answers[$progress];
        $futureChampion = Champion::firstWhere(['name' => $futureChampionName, 'active' => true]);

        // Security if champion is not found
        if (null == $futureChampion) {
            throw new Exception('ERROR: first champion >' . $futureChampionName . '< not found at index ' . $progress);
        } else {
            return view('champions.play', [
                'score' => $score,
                'championship' => $futureChampion->championship,
                'goal' => count($answers),
                'baseJersey' => $futureChampion->getJerseyUrl(),
                'leftJersey' => $futureChampion->getLeftJerseyUrl(),
                'rightJersey' => $futureChampion->getRightJerseyUrl(),
                'collarJersey' => $futureChampion->getCollarJerseyUrl(),
                'allSuggestionsJs' => session('suggestionsjs'),
            ]);
        }
    }

    /**
     * Restart a game with same championship and difficulty
     */
    public function retryGame(): RedirectResponse
    {
        $previousGameDbId = session('gameid');
        $previousGame = Game::find($previousGameDbId);
        $gameDbId = null;
        if ($previousGame) {
            $gameDbId = $this->createGameInDb($previousGame->ip, Championship::from($previousGame->championship), Difficulty::from($previousGame->difficulty));
        }
        $this->createGameInSession(session('championship'), session('difficulty'), $gameDbId);

        return redirect()->route('showGame');
    }

    /**
     * Receive an answer and returns a game status
     */
    public function championsPost(Request $request): JsonResponse
    {
        // Retrieve the list of correct answers
        $answers = session('answers');
        if (!$answers) {
            throw new Exception('ERROR: no game session started.');
        }

        // Retrieve progress in session
        $progress = session('progress');

        // Retrieve score in session
        $score = session('score');

        // Prefill status
        $returnStep = 'cont';
        $correct = 'no';

        // Check answer
        $answer = $request->input('answer');
        $wantedAnswer = $this->getWantedAnswer($progress, $answers);
        $isCorrect = $this->assertAnswerIsRight($answer, $wantedAnswer);

        // if the answer is correct
        if ($isCorrect) {
            // Set status correct
            $correct = 'yes';
            // Add one to score and save it
            $score += 1;
            session(['score' => $score]);
        }


        // Check game over
        $maximumProgress = count($answers);
        $progress += 1;

        // If game is over
        if ($progress >= $maximumProgress) {
            // Save game in DB
            $this->updateGameInDb($progress, $score, finished:true);

            // Compute star rating
            $stars = floor(5*$score/$maximumProgress);

            // Set message to copy
            $resultToCopy = __('tr.sharing-text', ['score' => $score]);

            // Empty session
            session(['answers' => null]);

            // Return game data
            return response()->json([
                'step' => 'over',
                'correct' => $correct,
                'message' => __('tr.over-message', ['score' => $score, 'goal' => $progress]),
                'resultToCopy' => $resultToCopy,
                'answer' => $wantedAnswer,
                'score' => $score,
                'stars'=> $stars,
            ]);
        }
        // if game continues
        else {

            // Update game status in DB
            $this->updateGameInDb($progress, $score);

            // Save in session next awaited answer index
            session(['progress' => $progress]);

            // Retrieve new champion to guess
            $futureChampionName = $answers[$progress];
            $futureChampion = Champion::firstWhere(['name' => $futureChampionName, 'active' => true]);

            // Security if champion is not found
            if (null == $futureChampion) {
                throw new Exception('ERROR: new champion >' . $futureChampionName . '< not found at index ' . $progress);
            } else {
                // Return game data
                return response()->json([
                    'step' => $returnStep,
                    'correct' => $correct,
                    'answer' => $wantedAnswer,
                    'score' => $score,
                    'championship' => $futureChampion->championship,
                    'baseJersey' => $futureChampion->getJerseyUrl(),
                    'leftJersey' => $futureChampion->getLeftJerseyUrl(),
                    'rightJersey' => $futureChampion->getRightJerseyUrl(),
                    'collarJersey' => $futureChampion->getCollarJerseyUrl(),
                ]);
            }
        }
    }

    /**
     * Create a new game in DB and session
     */
    protected function createGameInDbAndSession(Request $request): void
    {
        $dbGameId = $this->createGameInDb(
            $request->ip(),
            $request->enum('championship', Championship::class) ?? Championship::ALL,
            $request->enum('difficulty', Difficulty::class) ?? Difficulty::EASY,
        );
        $this->createGameInSession(
            $request->enum('championship', Championship::class) ?? Championship::ALL,
            $request->enum('difficulty', Difficulty::class) ?? Difficulty::EASY,
            $dbGameId,
        );
    }

    /**
     * Create all needed information to start a game and save it in session
     */
    protected function createGameInSession(Championship $championship = Championship::ALL, Difficulty $difficulty = Difficulty::EASY, int $dbGameId): void
    {
        // Create javascript of possible answers
        $allSuggestionsJs = $this->getAllSuggestionsJs($championship);
        // Put it in session
        session(['suggestionsjs' => $allSuggestionsJs]);

        // Get a shuffled list of champions
        $answers = $this->getAnswersForChampionshipAndDifficulty($championship, $difficulty);

        // Transform the answers with possible duplicates
        $duplicatedAnswers = $this->addDuplicatesToAnswers($answers);

        // Save in session the list of normalized correct answers
        session(['answers' => $duplicatedAnswers]);

        // Save championship and difficulty in session
        session(['championship' => $championship]);
        session(['difficulty' => $difficulty]);

        // Set first progress in session
        session(['progress' => 0]);

        // Set first score in session
        session(['score' => 0]);

        // Set game ID in session
        session(['gameid' => $dbGameId]);
    }

    /**
     * Retrieve answers for a set championship and difficulty
     */
    protected function getAnswersForChampionshipAndDifficulty(Championship $championship = Championship::ALL, Difficulty $difficulty = Difficulty::EASY): array
    {
        // Get a list of all champions of championship
        $champions = $this->getAllChampionsOrderedByRanking($championship);

        // Shuffle the list
        $shuffledChampions = $this->shuffleChampions($champions, $championship, $difficulty);

        // Return the answers
        return $shuffledChampions->pluck('name')->toArray();
    }

    /**
     * Get a list of champions of a championship ordered by name
     */
    protected function getAllChampionsOrderedByRanking(Championship $championship = Championship::ALL): Collection
    {
        // Get all active champions...
        $request = Champion::where('active', true);

        //... of championship.
        if (Championship::MEN == $championship || Championship::WOMEN == $championship) {
            $request->where('championship', $championship->value);
        }
        $request->orderBy('ranking', 'asc');
        return $request->get();
    }

    /**
     * Shuffle the list of answers according to the difficulty and their own ranking
     */
    protected function shuffleChampions(Collection $champions, Championship $championship, Difficulty $difficulty): Collection
    {
        // The champions are ordered by ranking, let's take a subset for each level
        $difficultyAnswersCount = $this->championsCount[$championship->value][$difficulty->value];
        $shortListCount = 2.5 * $difficultyAnswersCount;
        $champions = $champions->slice(0, $shortListCount);

        // Sort the champions by descending weight so that the algorithm is faster
        // From https://stackoverflow.com/a/11872928
        $champions->sortByDesc('ranking');

        $i = 0;
        $shuffled = new Collection();
        $totalChampions = count($champions);
        // For each shortlisted champion
        while ($i < $totalChampions) {
            // Get the sum of ranking
            $totalWeight = $champions->reduce(function (int $carry, Champion $item) {
                return $carry + $item->ranking;
            }, 0);

            // Get a random int between 1 and the sum
            $rand = mt_rand(1, $totalWeight);

            // Explore the collection to find the corresponding champions
            foreach ($champions as $index => $champ) {
                $rand -= $champ->ranking;
                // If we find the one
                if (0 >= $rand) {
                    // We select it
                    $shuffled->prepend($champ);
                    $champions->forget($index);
                    break;
                }
            }
            ++$i;
        }

        // Only keep the needed number
        $neededCount = min($totalChampions, $difficultyAnswersCount);
        $shuffled = $shuffled->slice(0, $neededCount);

        return $shuffled;
    }

    // Add other possible answers for each answer by checking duplicates
    protected function addDuplicatesToAnswers(array $names): array
    {
        $duplicatedAnswers = [];

        // For each drawn answer
        foreach ($names as $name) {
            // The answer itself is right
            $rightAnswers = [$name];

            // Retrieve champion from DB...
            $champ = Champion::firstWhere(['name' => $name]);

            // Find duplicates
            $dups = Champion::where([
                'championship' => $champ->championship,
                'team' => $champ->team,
                'full' => $champ->full,
                'left' => $champ->left,
                'right' => $champ->right,
                'collar' => $champ->collar,
                'active' => true,
                'duplicate' => true,
            ])
                ->whereNot(function ($query) use ($name) {
                    $query->where('name', $name);
                })
                ->get();

            // For each duplicates
            foreach ($dups as $otherChampion) {
                // Add their name to right answers
                $rightAnswers[] = $otherChampion->name;
            }

            // Add the right answers to the list of answers
            $duplicatedAnswers[] = $rightAnswers;
        }

        return $duplicatedAnswers;
    }

    /**
     * Create a new game in DB
     */
    protected function createGameInDb(?string $ip, Championship $championship = Championship::ALL, $difficulty = Difficulty::EASY): int
    {
        $game = new Game();
        $game->ip = $ip;
        $game->startDate = new DateTime();
        $game->championship = $championship;
        $game->difficulty = $difficulty;
        $game->step = 0;
        $game->score = 0;
        $game->save();

        return $game->id;
    }

    /**
     * Return the correct answer
     */
    protected function getWantedAnswer(int $progress, array $answers): ?array
    {
        if ($progress > count($answers)) {
            throw new Exception('ERROR : cannot find answer number ' . $progress);
        }

        return $answers[$progress];
    }

    /**
     * Assert a given answer is another answer
     */
    protected function assertAnswerIsRight(string $answer, array $correctAnswer): bool
    {
        // $normalizedAnswer = trim(strtolower($answer));
        return in_array($answer, $correctAnswer);
    }

    /**
     * Get a string representing a JS array of given suggestions
     */
    protected function getAllSuggestionsJs(Championship $championship = Championship::ALL): string
    {
        // Fetch all active champions...
        $champions = $this->getAllChampionsOrderedByRanking($championship);

        $allSuggestionsMen = "[";
        $allSuggestionsWomen = "[";
        foreach ($champions as $champion) {
            if (Championship::MEN->value == $champion->championship) {
                $allSuggestionsMen .= '"' . $champion->name . '", '; 
            } else {
                $allSuggestionsWomen .= '"' . $champion->name . '", ';
            }
        }
        if (strlen($allSuggestionsMen) > 1) {
            $allSuggestionsMen = substr($allSuggestionsMen, 0, -2);
            $allSuggestionsMen .= "]";
        } else {
            $allSuggestionsMen = '[]';
        }

        if (strlen($allSuggestionsWomen) > 1) {
            $allSuggestionsWomen = substr($allSuggestionsWomen, 0, -2);
            $allSuggestionsWomen .= "]";
        } else {
            $allSuggestionsWomen = '[]';
        }

        $allSuggestionsJs = "{men: " . $allSuggestionsMen . ", women: " . $allSuggestionsWomen . "}";

        return $allSuggestionsJs;
    }

    /**
     * Save in DB final information about an ended game
     */
    protected function updateGameInDb(int $progress, int $score, bool $finished = false): void
    {
        $game = Game::find(session('gameid'));
        if ($game) {
            $game->step = $progress;
            $game->score = $score;
            if ($finished) {
                $game->endDate = new DateTime();
            }
            $game->save();
        }
    }
}