<?php

namespace App\Http\Controllers;

use App\Models\Champion;
use Illuminate\View\View;

/**
 * Controller for the champions functions
 */
class ChampionController extends Controller
{

    /**
     * List all active champions
     */
    public function listAll(): View
    {
        // Fetch all active champions
        $champions = Champion::where('active', true)->orderBy('ranking', 'asc')->get();
        $totalCount = $champions->count();
        $womenCount = $champions->filter(function (Champion $champion) {
            return 'women' == $champion->championship;
        })->count();

        // Display view
        return view('champions.all', [
            'champions' => $champions,
            'womenCount' => $womenCount,
            'menCount' => $totalCount - $womenCount,
        ]);
    }
}