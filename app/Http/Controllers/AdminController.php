<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function dashboard()
    {
        $allGames = Game::where('ip', 'not like', '78.192.31.5')->get();
        $gamesCount = $allGames->count();
        $endedGamesCount = $allGames->whereNotNull('endDate')->count();
        $playersCount = $allGames->unique('ip')->count();

        $days = [];
        $minDay = Carbon::createFromDate(2024, 04, 02);
        $day = Carbon::today();

        // Fill the array with zeros
        do {
            $days[$day->format('Y-m-d')][false] = 0;
            $days[$day->format('Y-m-d')][true] = 0;
            $day = $day->subDay();
        } while ($day->isAfter($minDay));

        foreach ($allGames as $game) {
            $gameDate = (new Carbon($game->startDate))->format('Y-m-d');
            ++$days[$gameDate][(null != $game->endDate)];
        }

        return view('champions.dashboard', [
            'gamesCount' => $gamesCount,
            'endedGamesCount' => $endedGamesCount,
            'playersCount' => $playersCount,
            'days' => $days,
        ]);
    }
}