<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\Championship;
use App\Enums\Difficulty;
use Illuminate\Validation\Rules\Enum;

class StartGameRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'championship' => [new Enum(Championship::class)],
            'difficulty' => [new Enum(Difficulty::class)],
        ];
    }
}